#pragma once
#include "Composite.h"
#include "Coord.h"
#include "BoardBorder.h"
#include "Tile.h"
#include "GameComponent.h"
#include "Keyboard.h"
#include "IBoardRotationState.h"
#include "IGameWindow.h"

class Board : public Composite, public GameComponent
{
private:
	Coord2D size;
	BoardBorder* boardBorder;
	IGameWindow *gameWindow;

	const GLfloat ROTATION_INC; //degrees per 100ms
	const GLfloat MAX_ROTATION;
	const Coord2D TILE_MOVE_INC;    //move-distance per 100ms
	const GLfloat TILE_TRANSLATION_INC;
	IBoardRotationState *rotationState;
	IBoardRotationState *initialState;
	bool tilesMoved;

	TileField *tileFields;
	int tilesLength;

public:
	Board(IGameWindow *gameWindow, IBoardRotationState *initialState, Coord2D size = { 4, 4 });    //size: amount of tiles in x- and y-direction
	virtual ~Board();

	void update(float elapsedTime);

	void setState(IBoardRotationState *state);
	void setToInitialState();
	void addScore(int amount);
	GLfloat getRotationInc();
	GLfloat getMaxRotation();
	bool getTilesMoved();
	void setTilesMoved(bool value);
    Coord2D getTileMoveInc();
	GLfloat getTileTranslationInc();
	int getTilesLength();
	Coord2D getTileDimensions();
	TileField* getTileFields();
	int getTileIndex(int x, int y);

	void onGameWin();
	void onGameLoose();
	void removeAllTiles();

private:
	void initTiles();
};