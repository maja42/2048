#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <list>
#include "GlutWindow.h"
#include "Board.h"
#include "ScoreBox.h"
#include "CamBox.h"
#include "Skybox.h"
#include "EndBox.h"
#include "Coord.h"
#include "config.h"
#include "GameComponent.h"
#include "BoardMiddleState.h"
#include "IGameWindow.h"

class GameWindow : public GlutWindow, public IGameWindow
{
private:
    Board* board;
	Skybox* skyBox;
    ScoreBox* scoreBox;
    CamBox* cambox;
	std::list<GameComponent*> gameComponents;
	bool isFirst2048;
	EndBox *endBox;
	bool isGameOver;

    //Perspektiven-Animation:
    float angles[3], targetAngles[3];
    //float xAngle, xTargetAngle;
    //float zAngle, zTargetAngle;

    //These pointers are only references, in order to access the components easier:
    Composite *gameContainerX, *gameContainerY, *gameContainerZ;       //:( We need this, in order to support rotation about multiple axis
    Composite *drawingContainer3D, *drawingContainer2D;
public:
    GameWindow(Coord2D position = { 0, 0 }, Coord2D windowSize = { MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT }, const char* windowTitle = MAIN_WINDOW_TITLE);
    virtual ~GameWindow();


    void registerGameComponent(GameComponent* newComponent);
    void deregisterGameComponent(GameComponent* oldComponent);

	void addScore(int amount);
	void onGameWin();
	void onGameLoose();
	void update(float elapsedTime);
};

