// Based on public domain code by Antonio R. Fernandes <ajbrf@yahoo.com>

#include "tga.h"

Tga::Tga(const char* filePath, Coord2D spriteCount, Coord2D usedSprite){
    this->filePath = filePath;
    this->textureId = -1;
    this->status = TGA_NOT_LOADED;

    setSpriteCount(spriteCount.x, spriteCount.y);
    setUsedSprite(usedSprite.x, usedSprite.y);
}


Tga::~Tga(){
    destroy();
}

void Tga::destroy(){
    if (imageData != NULL) {
        free(imageData);
        imageData = NULL;
    }
}

void Tga::load(){
    if (status == TGA_OK){  //Texture already loaded
        return;
    }

    //open the file for reading (binary mode)
    FILE* file = fopen(filePath, "rb");
    if (file == NULL) {
        status = TGA_ERROR_FILE_OPEN;
        std::string error = "Error loading TGA-File: Unable to open file "; 
        error += filePath;
        throw std::exception(error.c_str());
    }
    clearerr(file);
    
    // load the header
    loadHeader(file);

    // check for errors when loading the header
    if (ferror(file)) {
        fclose(file);
        status = TGA_ERROR_READING_FILE;
        throw std::exception("Error loading TGA-File");
    }
        
    int mode = pixelDepth / 8;                                              // mode equals the number of image components
    int bytesToRead = height * width * mode;    
    if (imageData != NULL) { free(imageData); imageData = NULL; }
    imageData = (unsigned char *)malloc(sizeof(unsigned char)*bytesToRead); // allocate memory for image pixels

    // check to make sure we have the memory required
    if (imageData == NULL) {
        fclose(file);
        status = TGA_ERROR_MEMORY;
        throw std::exception("Error loading TGA-File");
    }
    // finally load the image pixels
    loadData(file);

    // check for errors when reading the pixels
    if (ferror(file)) {
        fclose(file);
        status = TGA_ERROR_READING_FILE;
        throw std::exception("Error loading TGA-File");
    }
    fclose(file);

    status = TGA_OK;    
    uploadToGPU();
    halfTexelSize = { 0.5f / width, 0.5f / height };    //Size of half a texel. Needed as a correction for sprites
    return; 
}


void Tga::uploadToGPU(){
    //Load the image on the GPU:
    GLuint textureId;
    glGenTextures(1, &textureId);								    //Eine neue unique Zahl von der Graka holen
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, textureId);						//Der Textur wird jetzt eine Zahl zugewiesen
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    GLint format = (pixelDepth/8 == 4) ? GL_RGBA : GL_RGB;

    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, imageData);	    //Laden des Bildes auf die Grafikkarte    
    destroy();													    //Bild aus dem Ram l�schen (es befindet sich noch auf der Grafikkarte)    
    this->textureId = textureId;
}


GLint Tga::getTextureId(){
    if (status == TGA_NOT_LOADED){  //Lazy-Load!
        load();
    }
    if (status != TGA_OK){
        throw std::exception("Unable to load Texture -> can't return textureId");
    }
    return textureId;
}


void Tga::loadHeader(FILE* file){
    unsigned char cGarbage;
    short int iGarbage;

    fread(&cGarbage, sizeof(unsigned char), 1, file);
    fread(&cGarbage, sizeof(unsigned char), 1, file);

    // type must be 2 or 3
    unsigned char type;
    fread(&type, sizeof(unsigned char), 1, file);

    // check if the image is color indexed
    if (type == 1) {
        status = TGA_ERROR_INDEXED_COLOR;
        fclose(file);
        throw std::exception("Error loading TGA-File, it's color-indexed");
    }

    fread(&iGarbage, sizeof(short int), 1, file);
    fread(&iGarbage, sizeof(short int), 1, file);
    fread(&cGarbage, sizeof(unsigned char), 1, file);
    fread(&iGarbage, sizeof(short int), 1, file);
    fread(&iGarbage, sizeof(short int), 1, file);

    fread(&width, sizeof(short int), 1, file);
    fread(&height, sizeof(short int), 1, file);
    fread(&pixelDepth, sizeof(unsigned char), 1, file);
    
    #if 0 // debug
        printf("Image dimensions: %d %d %d\n", width, height, pixelDepth);
    #endif

    fread(&cGarbage, sizeof(unsigned char), 1, file);
}


void Tga::loadData(FILE *file){ // loads the image pixels
    int mode, total, i;
    unsigned char aux;

    // mode equal the number of components for each pixel
    mode = pixelDepth / 8;
    // total is the number of bytes we'll have to read
    total = height * width * mode;

    fread(imageData, sizeof(unsigned char), total, file);

    #if 0 // debug
        printf("Image mode: %d\n", mode);
    #endif

    // mode=3 or 4 implies that the image is RGB(A). However TGA
    // stores it as BGR(A) so we'll have to swap R and B.
    if (mode >= 3)
    for (i = 0; i < total; i += mode) {
        aux = imageData[i];
        imageData[i] = imageData[i + 2];
        imageData[i + 2] = aux;
    }
}

void Tga::setSpriteCount(int s, int t){  //s = x; t = y
    if (s <= 0 || t <= 0){
        throw std::exception("Invalid Sprite Count");
    }
    spriteCount = { s, t };
    spriteSize = { 1.0 / s, 1.0 / t };
}

void Tga::setUsedSprite(int s, int t){       //Defines the sprite, which will be used for output
    if (s < 0 || t < 0 || s >= spriteCount.x || t >= spriteCount.y){
        throw std::exception("Invalid Sprite-Number. Value out of bounds.");
    }
    usedSprite.x = s;
    usedSprite.y = spriteCount.y - t -1;
}

Coord2D Tga::getSpriteCount(){
    return spriteCount;
}

Coord2D Tga::getTextureCoordinates(Coord2D texCoord){    //Returns the correct texture coordinates, which care about the used sprite
    return{ spriteSize.x* (usedSprite.x + texCoord.x) + halfTexelSize.x - (2 * texCoord.x * halfTexelSize.x),
            spriteSize.y* (usedSprite.y + texCoord.y) + halfTexelSize.y - (2 * texCoord.y * halfTexelSize.y) };
}
