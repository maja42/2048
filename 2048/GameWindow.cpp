#include "GameWindow.h"


GameWindow::GameWindow(Coord2D position, Coord2D windowSize, const char* windowTitle) : GlutWindow(position, windowSize, windowTitle, COLOR_BLACK){
    Coord2D size = BOARD_SIZE;
    //Generate References for easy-access:
        gameContainerZ = renderer->get3DWorld();
        gameContainerY = new Composite();
        gameContainerX = new Composite();
        gameContainerZ->addRenderable(gameContainerY);
        gameContainerY->addRenderable(gameContainerX);
        drawingContainer3D = gameContainerX;
        drawingContainer2D = renderer->get2DOverlay();

    //Add the drawable elements:
	    this->board = new Board(this, new BoardMiddleState(), size);
        drawingContainer3D->addRenderable(this->board);
        this->gameComponents.push_back(this->board);

		this->skyBox = new Skybox();
        drawingContainer3D->addRenderable(this->skyBox);
        skyBox->setScaling(200, 200, 200);
        skyBox->setTranslation(0, -120, -40);

        this->scoreBox = new ScoreBox();
        drawingContainer2D->addRenderable(this->scoreBox);
        this->gameComponents.push_back(this->scoreBox);

        this->cambox = new CamBox(2);
        drawingContainer2D->addRenderable(this->cambox);
        this->gameComponents.push_back(this->cambox);
        
		this->isGameOver = false;
		this->isFirst2048 = true;

		this->endBox = NULL;


    //Add lightning:
        renderer->addLightSource(new LightSource({
            /*position*/{ 0.0, 5.0, 10, 0.0 },      //
            /*diffuse*/ { 0.6, 0.6, 0.6, 1.0 },     //Gerichtetes Licht
            /*ambient*/ { 1.0, 1.0, 1.0, 1.0 }      //Umgebungslicht
        }));
        renderer->addLightSource(new LightSource({
            /*position*/{ 10, 0.0, 10, 0.0 },       //
            /*diffuse*/ { 0.2, 0.2, 0.2, 1.0 },     //Gerichtetes Licht
            /*ambient*/ { 1.0, 1.0, 1.0, 1.0 }      //Umgebungslicht
        }));
          


    //Perspektive initialisieren:
    angles[0] = 0; angles[1] = 0; angles[2] = 0;
    targetAngles[0] = 64; targetAngles[1] = 0; targetAngles[2] = 0;

    

    //Von der Kamera wegschieben:
    float moveAwayValue = 6;
    int maxBoardSize = (size.x>size.y)?size.x:size.y;
    moveAwayValue = maxBoardSize * 1.55;

    drawingContainer3D->setTranslation(0, 0, -moveAwayValue);




    //Die Kamera �ber das Spielfeld drehen:
    gameContainerX->setRotation(X_AXIS, angles[0]);
    gameContainerY->setRotation(Y_AXIS, angles[1]);
    gameContainerZ->setRotation(Z_AXIS, angles[2]);

}


GameWindow::~GameWindow(){
    delete board;
	delete skyBox;
}


void GameWindow::update(float elapsedTime)
{
    for (std::list<GameComponent*>::iterator i = this->gameComponents.begin(); i != this->gameComponents.end(); ++i){
		(*i)->update(elapsedTime);
	}

    Keyboard* input = Keyboard::GetInstance();
    
	if (this->isGameOver && input->IsKeyPressed(AK_Space))
	{
		this->scoreBox->addPoints(-(this->scoreBox->getScore()));
		this->board->removeAllTiles();
		delete(this->endBox);
		this->endBox = NULL;
		this->isGameOver = false;
	}

    if (angles[0] == targetAngles[0] && angles[1] == targetAngles[1] && angles[2] == targetAngles[2]){
        if (input->IsKeyPressed(AK_1)){
            targetAngles[0] = 30; targetAngles[1] = 0; targetAngles[2] = 0;
            cambox->setCam(1);
        }
        else
        if (input->IsKeyPressed(AK_2)){
            targetAngles[0] = 64; targetAngles[1] = 0; targetAngles[2] = 0;
            cambox->setCam(2);
        }
        else
        if (input->IsKeyPressed(AK_3)){
            targetAngles[0] = 90; targetAngles[1] = 0; targetAngles[2] = 0;
            cambox->setCam(3);
        }
        else if (input->IsKeyPressed(AK_4)){
            targetAngles[0] = 50; targetAngles[1] = -5; targetAngles[2] = 10;
            cambox->setCam(4);
        }
        else if (input->IsKeyPressed(AK_5)){
            targetAngles[0] = 50; targetAngles[1] = 5; targetAngles[2] = 30;
            cambox->setCam(5);
        }
        else if (input->IsKeyPressed(AK_6)){
            targetAngles[0] = 30; targetAngles[1] = 15; targetAngles[2] = -10;
            cambox->setCam(6);
        }
        else if (input->IsKeyPressed(AK_7)){
            targetAngles[0] = 100; targetAngles[1] = -8; targetAngles[2] = 12;
            cambox->setCam(7);
        }
        else if (input->IsKeyPressed(AK_8)){
            targetAngles[0] = 50; targetAngles[1] = -5; targetAngles[2] = -10;
            cambox->setCam(8);
        }
        else if (input->IsKeyPressed(AK_9)){
            targetAngles[0] = 150; targetAngles[1] = 0; targetAngles[2] = 179;
            cambox->setCam(9);
        }
        else if (input->IsKeyPressed(AK_0)){
            targetAngles[0] = 80; targetAngles[1] = 0; targetAngles[2] = -20;
            cambox->setCam(0);
        }
    }

    bool changes = false;
    for (int i = 0; i < 3; i++){
        if (angles[i] < targetAngles[i]){
            angles[i] += 3;
            if (angles[i] >= targetAngles[i]){
                angles[i] = targetAngles[i];
            }
            changes = true;
        }
        if (angles[i] > targetAngles[i]){
            angles[i] -= 3;
            if (angles[i] <= targetAngles[i]){
                angles[i] = targetAngles[i];
            }
            changes = true;
        }
    }

    if (changes){
        gameContainerX->setRotation(X_AXIS, angles[0]);
        gameContainerY->setRotation(Y_AXIS, angles[1]);
        gameContainerZ->setRotation(Z_AXIS, angles[2]);
    }

    char str[128] = "";
    sprintf(str, "2048 - %.1ffps; Meshes to render: %d", 1000.0f / elapsedTime, renderer->getRenderedMeshes());
    glutSetWindowTitle(str);

    if(Keyboard::GetInstance()->IsKeyPressed(AK_ESC)){  //Spiel beenden
        exit(0);
    }
}

void GameWindow::addScore(int amount)
{
	this->scoreBox->addPoints(amount);
}

void GameWindow::registerGameComponent(GameComponent* newComponent){
    gameComponents.push_back(newComponent);
}

void GameWindow::deregisterGameComponent(GameComponent* oldComponent){
    gameComponents.remove(oldComponent);
}

void GameWindow::onGameWin()
{
	if (this->isFirst2048)
	{
		if (this->endBox == NULL)
		{
			this->endBox = new EndBox(true);
			drawingContainer2D->addRenderable(this->endBox);
			this->gameComponents.push_back(this->endBox);
		}
		this->isFirst2048 = false;
	}
}

void GameWindow::onGameLoose()
{
	if (this->endBox == NULL)
	{
		this->endBox = new EndBox(false);
		drawingContainer2D->addRenderable(this->endBox);
		this->gameComponents.push_back(this->endBox);
		this->isGameOver = true;
	}
}