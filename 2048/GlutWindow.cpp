#include "GlutWindow.h"



GlutWindow::GlutWindow(Coord2D position, Coord2D size, const char* windowTitle, Color clearColor){
    this->size = size;
    this->windowSize = size;
    this->clearColor = clearColor;

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);     //RGBA, double buffering
    glutInitWindowSize(size.x, size.y);                                         //Fenstergr��e
    glutInitWindowPosition(position.x, position.y);                             //Position des Fensters am Bildschirm
    windowID = glutCreateWindow(windowTitle);                                   //Neues Fenster erzeugen
    renderer = new Renderer(windowID);                                          //Einen Renderer f�r dieses Window erzeugen  (will be deleted in destructor)


    glClearColor(clearColor.r, clearColor.g, clearColor.b, 0.0f);
    glClearDepth(1.0);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);                                                    //Better Shading (Interpolate between shapes)
    glEnable(GL_BLEND);                                                         //
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_NORMALIZE);                                                     //We don't wanna care about normal-vector lengths

    glAlphaFunc(GL_GREATER, 0.2f);                                             //Ignore all Alpha-Values less than 0.05
    glEnable(GL_ALPHA_TEST);                                                    //Enable Alpha-testing (look through meshes)

    //resize(size);

    WindowManager::getInstance()->registerWindow(this);     //Register this Window      
}


GlutWindow::~GlutWindow(){
    delete renderer;
    renderer = NULL;
    glutDestroyWindow(windowID);                            //This function might fire the glutEntryFunc(); --> we have to keep the window registered until we destroyed it
    WindowManager::getInstance()->deregisterWindow(this);
}




void GlutWindow::resize(Coord2D newSize){                   //Wird aufgerufen, wenn dieses Fenster resized wird
    if (newSize.y == 0) { newSize.y = 1; }
    windowSize = newSize;
}

void GlutWindow::setCursor(int cursor){                                             //�ndert den Cursor in diesem Fenster
    //glutSetWindow(windowID);
    glutSetCursor(cursor);
}

void GlutWindow::render() const{                                                    //Wird beim neu zeichnen aufgerufen
    renderer->renderAll(windowSize, size);
}



void GlutWindow::mouseFunc(int button, int state, Coord2D position){                //Wird bei einem Tastendruck der Maus innerhalb des Fensters aufgerufen
}

void GlutWindow::mouseMotionFunc(Coord2D position){                                 //Wird bei der Mausbewegung innerhalb des Fensters aufgerufen
}

void GlutWindow::mousePassiveMotionFunc(Coord2D position){                          //Wird bei der Mausbewegung innerhalb des Fensters aufgerufen
}

void GlutWindow::mouseEntryFunc(int state){                                         //Wird aufgerufen, wenn das Fenster den Fokus verliert oder bekommt
}

int GlutWindow::getWindowID(){
    return windowID;
}

Coord2D GlutWindow::getSize(){            //Returns the size of the coord.-system (is NOT the window-size)
    return size;
}
