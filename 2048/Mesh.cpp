#include "Mesh.h"


Mesh::Mesh(Material* front, Material* back, GLint texture){
    this->front = front;
    this->back = back;
    this->texture = texture;    
	this->normal = new Coord3D({ 0.0, 1.0, 0.0 });
}


Mesh::Mesh(const Mesh& copyMe) : Renderable(copyMe){             //Overwrite the copy-Constructor to perform a deepCopy
    setRenderMode(copyMe.getRenderMode());
    front = copyMe.front;
    back = copyMe.back;
    texture = copyMe.getTexture();
	normal = copyMe.getNormal();

    std::vector<Vertex*> const* vertices = copyMe.getVertices();
    for (std::vector<Vertex*>::const_iterator iter = vertices->begin(); iter != vertices->end(); ++iter){
        addVertex(new Vertex(**iter));
    }
}



Mesh::~Mesh(){
    if (front != NULL){
        //delete front;
        front = NULL;
    }
    if (back != NULL){
        //delete back;
        back = NULL;
    }
	if (normal != NULL){
		//delete normal;
        normal = NULL;
	}
}

void Mesh::setMaterial(Face face, Material* material){
    if (face == FRONT){
        if (front != NULL){
            //delete front;
        }
        front = material;
    }
    else{
        if (back != NULL){
            //delete back;
        }
        back = material;
    }
}

const Material* Mesh::getMaterial(Face face){
    if (face == FRONT){
        return front;
    }else{
        return back;
    }
}

void Mesh::setRenderMode(GLenum mode){
    renderMode = mode;
}

void Mesh::setColor(Color color){
    for (std::vector<Vertex*>::iterator iter = vertices.begin(); iter != vertices.end(); ++iter){
        (*iter)->setColor(color);
    }
    if (color.a >= 0){
        if (front != NULL){
            setTransparency(front, color.a);
        }
        if (back != NULL){
            setTransparency(back, color.a);
        }
    }    
}








void Mesh::setNormal(Coord3D* normal){
	if (normal == NULL){
		this->normal = new Coord3D({ 0.0, 1.0, 0.0 });
	}
	else{
		this->normal = normal;
	}
	
}

Coord3D* Mesh::getNormal() const{
	return normal;
}

void Mesh::addVertex(Vertex* newVertex){
    vertices.push_back(newVertex);
}

void Mesh::clearVertices(){
    for (std::vector<Vertex*>::iterator iter = vertices.begin(); iter != vertices.end(); ++iter){
        delete *iter;
    }
    vertices.clear();
}

bool Mesh::isEmpty() const{
    return vertices.empty();
}

GLenum Mesh::getRenderMode() const{
    return renderMode;
}

std::vector<Vertex*> const * Mesh::getVertices() const{
    return &vertices;
}

GLint Mesh::getTexture() const{
    return texture;
}