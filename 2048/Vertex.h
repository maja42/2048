#pragma once

#include <stdlib.h>

#include "Coord.h"
#include "Color.h"

class Vertex
{
private:
    Coord3D position;
    
    Color color;
    Coord2D textureCoord;
public:
    Vertex(Coord3D position, Coord2D textureCoord = { 0, 0 }, Color color = COLOR_WHITE);

    virtual ~Vertex();

    void setPosition(Coord3D position);
    Coord3D getPosition();

    void setColor(Color color);
    Color getColor();

    void setTextureCoordinates(Coord2D textureCoord);
    Coord2D getTextureCoordinates();
};

