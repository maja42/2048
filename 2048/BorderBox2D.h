#pragma once
#include <iostream>

#include "Composite.h"
#include "Mesh.h"
#include "Coord.h"
#include "Tga.h"



enum BorderBox2dTypes{
    WHITE_BORDER_ROUND = 0,     //Transparenter Hintergrund
    BRIGHT_BOX = 1,
    DARK_BOX = 2,
    BLACK_BOX = 3,
    WHITE_BORDER = 4            //Transparenter Hintergrund
};


class BorderBox2D : public Composite
{
private:
    static Tga* boxSprites;
public:
    BorderBox2D(Coord2D pos, Coord2D size, GLfloat borderWidth, BorderBox2dTypes boxType);    
    virtual ~BorderBox2D();
};

