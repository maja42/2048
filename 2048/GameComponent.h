#pragma once

class GameComponent
{
public:
	virtual void update(float elapsedTime) = 0;
};