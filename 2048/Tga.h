#pragma once
// Based on public domain code by Antonio R. Fernandes <ajbrf@yahoo.com>

#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <GL/glu.h>
#include "glut.h" 

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <exception>

#include "Coord.h"

#define NO_TEXTURE -1

enum TgaStatus{
    TGA_ERROR_FILE_OPEN,
    TGA_ERROR_READING_FILE,
    TGA_ERROR_INDEXED_COLOR,
    TGA_ERROR_MEMORY,
    TGA_ERROR_COMPRESSED_FILE,
    TGA_NOT_LOADED,
    TGA_OK
};


class Tga
{
private:
    const char* filePath;

    GLint textureId;                //ID of the texture on the Graka

    Coord2D spriteCount;            //Number of sprites in this image
    Coord2D usedSprite;             //The Sprite, which will be used

    Coord2D spriteSize;             //The size of a single Sprite in Texture-Coordinates
    Coord2D halfTexelSize;          //The size of half a texel in Texture-Coordinates

    TgaStatus status;
    unsigned char pixelDepth;       //number of bits per Pixel
    short int width, height;        //Imagesize
    unsigned char *imageData;       //Binary imagedata

    void loadHeader(FILE* file);
    void loadData(FILE *file);
    void uploadToGPU();
    void destroy();
public:
    Tga(const char* filePath, Coord2D spriteCount = { 1, 1 }, Coord2D usedSprite = { 0, 0 });
    ~Tga();
    void load();

    void setSpriteCount(int s, int t);          //s = x; t = y
    void setUsedSprite(int s, int t);           //Defines the sprite, which will be used for output
    Coord2D getSpriteCount();
    Coord2D getTextureCoordinates(Coord2D texCoord);    //Returns the correct texture coordinates, which care about the used sprite

    GLint getTextureId();
};
