#include "Coord.h"



bool operator == (const Coord2D& a, const Coord2D& b) {
    return std::tie(a.x, a.y) == std::tie(b.x, b.y);
}

bool operator != (const Coord2D& a, const Coord2D& b) {
    return std::tie(a.x, a.y) != std::tie(b.x, b.y);
}


GLfloat distance(const Coord2D& a, const Coord2D& b){
    Coord2D diff = { a.x - b.x, a.y - b.y };
    return sqrtf(diff.x*diff.x + diff.y*diff.y);
}




bool operator == (const Coord3D& a, const Coord3D& b) {
    return std::tie(a.x, a.y, a.z) == std::tie(b.x, b.y, b.z);
}

bool operator != (const Coord3D& a, const Coord3D& b) {
    return std::tie(a.x, a.y, a.z) != std::tie(b.x, b.y, b.z);
}


GLfloat distance(const Coord3D& a, const Coord3D& b){
    Coord3D diff = { a.x - b.x, a.y - b.y };
    return sqrtf(diff.x*diff.x + diff.y*diff.y + diff.z*diff.z);
}


