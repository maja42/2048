#pragma once

#include <stdlib.h>
#include <list>

#include "Coord.h"
#include "GlutWindow.h"
#include "Mesh.h"


class ShapeFactory
{
    ShapeFactory();
    static ShapeFactory* singleton;
public:
    static ShapeFactory* getInstance();                            //Gibt die Singleton-Instanz zur�ck (Lazy Initialisierung)
	~ShapeFactory();

	Mesh* newQuad(Material* front, Material* back, GLint texture, Vertex* v1, Vertex* v2, Vertex* v3, Vertex* v4);
	Mesh* newTriangle(Material* front, Material* back, GLint texture, Vertex* v1, Vertex* v2, Vertex* v3);
};

