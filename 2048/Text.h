#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#include "Mesh.h"
#include "Color.h"
#include "Tga.h"
#include "Composite.h"




struct TextSymbol{          //Einzelnes Zeichen eines fonts
    const char symbol;      //Zeichen (zB. 'a', 'b', '0', '@', ...)
    const float width;      //Zeichenbreite (0.0 - 1.0)
};


class Text : public Composite
{

private:
    char text[32];
    Color color;
    GLfloat textSize;
    GLfloat textWidth;      

    GLfloat fontRatio;
    static Tga* fontSprites;
    static const TextSymbol symbols[11][10];



    void updateMesh();                            //Generates the output again
    GLfloat addSymbol(char letter, GLfloat xPos);   //Adds another letter to the output, and returns the new x-Pos
public:
    Text(const char* text, GLfloat textSize, Color color = COLOR_WHITE);
    virtual ~Text();

    void setText(const char* text);
    void setFontSize(GLfloat textSize);
    void setColor(Color color);

    GLfloat getTextWidth();     //Returns the width of the printed text    
};

