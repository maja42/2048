#pragma once

#include "IBoardRotationState.h"
#include "BoardLeftState.h"
#include "BoardRightState.h"
#include "BoardUpState.h"
#include "BoardDownState.h"
#include "Board.h"
#include "Keyboard.h"

class BoardMiddleState : public IBoardRotationState
{
private:
	GLfloat _TileY;
	const GLfloat TILE_MIN_Y;
	short _Sign;
	bool _IsAtBottom;
	bool _MergeCompleted;
	bool _IsFirstUpdate;

public:
	BoardMiddleState();
	virtual ~BoardMiddleState();

    void UpdateLogic(Board *context, float elapsedTime);

private:
    void MergeTiles(Board *context, float elapsedTime);
	void ResetVariables(Board *context);
	bool CheckForMerge(TileField *fields, int length);
	void CreateTileAtRandom(Board *context);
	bool IsFieldFull(TileField *fields, int length);
	bool IsAnyTileMoveable(Board *context);
};