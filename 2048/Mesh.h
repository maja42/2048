#pragma once

#include <vector>

#include "Renderable.h"
#include "Vertex.h"
#include "Material.h"
#include <math.h>
#include <iostream>


class Mesh : public Renderable
{
private:
    GLenum renderMode;                              //modus f�r glBegin()
    std::vector<Vertex*> vertices;
	Coord3D* normal;

    Material* front;
    Material* back;

    GLint texture;
public:
    Mesh(Material* front = NULL, Material* back = NULL, GLint texture = -1);
    Mesh::Mesh(const Mesh& copyMe);                 //Overwrite the copy-Constructor to perform a deepCopy
    virtual ~Mesh();
    
    
    void setMaterial(Face face, Material* material);
    const Material* getMaterial(Face face);

    void setRenderMode(GLenum mode);
    void addVertex(Vertex* newVertex);
    void clearVertices();

    void setColor(Color color);
    void setMaterial(const Material* newMaterial);

	void setNormal(Coord3D* normal);
	Coord3D* getNormal() const;

    bool isEmpty() const;
    GLenum getRenderMode() const;
    std::vector<Vertex*> const* getVertices() const;
    GLint getTexture() const;
};

