#include "ShapeFactory.h"


ShapeFactory* ShapeFactory::singleton = NULL;


ShapeFactory::ShapeFactory(){
}



ShapeFactory* ShapeFactory::getInstance(){                           //Gibt die Singleton-Instanz zur�ck (Lazy Initialisierung)
    if (ShapeFactory::singleton == NULL){
        ShapeFactory::singleton = new ShapeFactory();       
    }
    return ShapeFactory::singleton;
}


Mesh* ShapeFactory::newQuad(Material* front, Material* back, GLint texture, Vertex* v1, Vertex* v2, Vertex* v3, Vertex* v4)
{
	Coord3D A = v1->getPosition();
	Coord3D B = v2->getPosition();
	Coord3D C = v3->getPosition();
	Coord3D D = v4->getPosition();

	//V1 = (C - A)
	//V2 = (D - B)

	Coord3D V1 = { C.x - A.x, C.y - A.y, C.z - A.y };
	Coord3D V2 = { D.x - B.x, D.y - B.y, D.z - B.y };

	Coord3D* normal = new Coord3D();

	normal->x = V1.y * V2.z - V1.z * V2.y;
	normal->y = V2.x * V1.z - V2.z * V1.x;
	normal->z = V1.x * V2.y - V1.y * V2.x;
	

	Mesh* quad = new Mesh(front, back, texture);
	quad->setNormal(normal);
	quad->setRenderMode(GL_QUADS);
	quad->addVertex(v1);
	quad->addVertex(v2);
	quad->addVertex(v3);
	quad->addVertex(v4);

	return quad;
}

Mesh* ShapeFactory::newTriangle(Material* front, Material* back, GLint texture, Vertex* v1, Vertex* v2, Vertex* v3)
{
	Coord3D A = v1->getPosition();
	Coord3D B = v2->getPosition();
	Coord3D C = v3->getPosition();
	
	//V1 = (B - A)
	//V2 = (C - A)

	Coord3D V1 = { B.x - A.x, B.y - A.y, B.z - A.y };
	Coord3D V2 = { C.x - A.x, C.y - A.y, C.z - A.y };

	Coord3D* normal = new Coord3D();

	normal->x = V1.y * V2.z - V1.z * V2.y;
	normal->y = V2.x * V1.z - V2.z * V1.x;
	normal->z = V1.x * V2.y - V1.y * V2.x;


	Mesh* quad = new Mesh(front, back, texture);
	quad->setNormal(normal);
	quad->setRenderMode(GL_TRIANGLES);
	quad->addVertex(v1);
	quad->addVertex(v2);
	quad->addVertex(v3);

	return quad;
}



ShapeFactory::~ShapeFactory(){
    delete ShapeFactory::singleton;
}
