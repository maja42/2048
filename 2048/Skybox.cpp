#include "Skybox.h"

Coord2D Skybox::boardSize = BOARD_SIZE;

Skybox::Skybox()
{
	this->scale = scale;
	ShapeFactory* shapeFactory = ShapeFactory::getInstance();
	Material* skyMat = new Material(DEFAULT_MATERIAL);


	skyBoxTexture = new Tga("data/skyboxBlue.tga");

    //Front
    Vertex* tl = new Vertex({ -0.5, 0, -0.5 }, { 0.0f, 0.0f });
    Vertex* tr = new Vertex({ -0.5, 0.62, -0.5 }, { 0.0f, 1.0f });
    Vertex* br = new Vertex({ +0.5, 0.62, -0.5 }, { 1.0f, 1.0f });
    Vertex* bl = new Vertex({ +0.5, 0, -0.5 }, { 1.0f, 0.0f });
    Mesh* front = shapeFactory->newQuad(skyMat, skyMat, skyBoxTexture->getTextureId(), tl, tr, br, bl);
    addRenderable(front);


    //Left
    tl = new Vertex({ -0.5, 0, -0.5 }, { 1.0f, 0.0f });
    tr = new Vertex({ -0.5, 0.62, -0.5 }, { 1.0f, 1.0f });
    br = new Vertex({ -0.5, 0.62, +0.5 }, { 0.0f, 1.0f });
    bl = new Vertex({ -0.5, 0, +0.5 }, { 0.0f, 0.0f });
    Mesh* left = shapeFactory->newQuad(skyMat, skyMat, skyBoxTexture->getTextureId(), tl, tr, br, bl);
    addRenderable(left);


    //Right
    tl = new Vertex({ +0.5, 0, -0.5 }, { 1.0f, 0.0f });
    tr = new Vertex({ +0.5, 0.62, -0.5 }, { 1.0f, 1.0f });
    br = new Vertex({ +0.5, 0.62, +0.5 }, { 0.0f, 1.0f });
    bl = new Vertex({ +0.5, 0, +0.5 }, { 0.0f, 0.0f });
    Mesh* right = shapeFactory->newQuad(skyMat, skyMat, skyBoxTexture->getTextureId(), tl, tr, br, bl);
    addRenderable(right);
	
    //Back
	tl = new Vertex({ -0.5, 0, +0.5 }, { 0.0f, 1.0f });
	tr = new Vertex({ -0.5, 0.62, +0.5 }, { 0.0f, 0.0f });
	br = new Vertex({ +0.5, 0.62, +0.5 }, { 1.0f, 0.0f });
    bl = new Vertex({ +0.5, 0, +0.5 }, { 1.0f, 1.0f});
    Mesh* back = shapeFactory->newQuad(skyMat, skyMat, skyBoxTexture->getTextureId(), tl, tr, br, bl);
    addRenderable(back);
	
    //Floor
    tl = new Vertex({ -0.5, 0, -0.5 }, { 0.0f, 0.0f });
    tr = new Vertex({ -0.5, 0, +0.5 }, { 0.0f, 1.f });
    br = new Vertex({ +0.5, 0, +0.5 }, { 1.0f, 1.0f });
    bl = new Vertex({ +0.5, 0, -0.5 }, { 1.0f, 0.0f });
	
	Mesh* floor = shapeFactory->newQuad(skyMat, skyMat, skyBoxTexture->getTextureId(), tl, tr, br, bl);
	addRenderable(floor);    
}


Skybox::~Skybox()
{
	delete skyBoxTexture;
}
