#pragma once

class IGameWindow
{
public:
	virtual void addScore(int amount) = 0;

	virtual void onGameWin() = 0;
	virtual void onGameLoose() = 0;
};