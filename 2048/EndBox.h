#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "config.h"
#include "Composite.h"
#include "BorderBox2D.h"
#include "Text.h"
#include "GameComponent.h"



class EndBox : public Composite, public GameComponent
{
private:
    Coord2D boxSize;

    int camNr;
    Text* message;
    BorderBox2D* endbox;

    bool fadeDirection;
    float fadeProgress;
public:
    EndBox(bool victory);
    virtual ~EndBox();

    bool isFadeEffectFinished();
    void fadeOut();

    void update(float elapsedTime);
};

