#include "Tile.h"

Coord2D Tile::boardSize = BOARD_SIZE;








Tile::Tile(int value, Coord2D position){  
    ShapeFactory* shapeFactory = ShapeFactory::getInstance();
    this->value = value;

    Color tileColor = Tile::getTileColor(value);
    Color labelColor = Tile::getLabelColor(value);

    Material* tileMat = new Material(TILE_MATERIAL);

    //tileMaterial an tileColor anpassen:
    tileMat->ambient[0] *= tileColor.r;
    tileMat->ambient[1] *= tileColor.g;
    tileMat->ambient[2] *= tileColor.b;

    //***************************** label ************************************
    float fontSize = 0.7;
    if (value >= 16192){
        fontSize = 0.30;
    }
    else
    if (value >= 1024){
        fontSize = 0.35;
    }
    else
    if (value >= 128){
        fontSize = 0.45;
    }
    else
    if (value >= 16){
        fontSize = 0.6;
    }
    char txt[32];
    sprintf(txt, "%d", value);
    


    label = new Text(txt, fontSize, labelColor);
    label->setRotation(X_AXIS, 270);
    label->setTranslation(-label->getTextWidth() / 2, TILE_HEIGHT + 0.3, fontSize/2);
    addRenderable(label);
    

    



    
    //***************************** top ************************************

    Vertex* bl = new Vertex({ -0.5 + TILE_BEVEL, TILE_HEIGHT, +0.5 - TILE_BEVEL }, { 0.0f, 0.0f });
    Vertex* br = new Vertex({ +0.5 - TILE_BEVEL, TILE_HEIGHT, +0.5 - TILE_BEVEL }, { 1.0f, 0.0f });
    Vertex* tr = new Vertex({ +0.5 - TILE_BEVEL, TILE_HEIGHT, -0.5 + TILE_BEVEL }, { 1.0f, 1.0f });
    Vertex* tl = new Vertex({ -0.5 + TILE_BEVEL, TILE_HEIGHT, -0.5 + TILE_BEVEL }, { 0.0f, 1.0f });
	
    Mesh* top = shapeFactory->newQuad(tileMat, NULL, NO_TEXTURE, bl, br, tr, tl);
    addRenderable(top);
    setPosition(position);

    

    //***************************** topEdges ************************************

        tl = new Vertex({ -0.5 + TILE_BEVEL, TILE_HEIGHT, +0.5 - TILE_BEVEL }, { 0.0f, 0.0f });
        tr = new Vertex({ +0.5 - TILE_BEVEL, TILE_HEIGHT, +0.5 - TILE_BEVEL }, { boardSize.x, 0.0f });
        br = new Vertex({ +0.5 - TILE_BEVEL, TILE_HEIGHT - TILE_BEVEL, +0.5 }, { 1.0f, 1.0f });
        bl = new Vertex({ -0.5 + TILE_BEVEL, TILE_HEIGHT - TILE_BEVEL, +0.5 }, { 0.0f, 1.0f });
    	
        Mesh* topEdge = shapeFactory->newQuad(tileMat, NULL, NO_TEXTURE, bl, br, tr, tl);
        addRenderable(topEdge);
        for (int i = 1; i < 4; i++){
            topEdge = new Mesh(*topEdge);
            topEdge->setRotation(Y_AXIS, i * 90);
            addRenderable(topEdge);
        }

            
    
    //***************************** wall ************************************
            
        tr = new Vertex({ +0.5 - TILE_BEVEL, TILE_HEIGHT - TILE_BEVEL, +0.5 }, { 1.0f, 1.0f });
        tl = new Vertex({ -0.5 + TILE_BEVEL, TILE_HEIGHT - TILE_BEVEL, +0.5 }, { 0.0f, 1.0f });
        br = new Vertex({ +0.5 - TILE_BEVEL, 0.0f, +0.5 }, { 1.0f, 1.0f });
        bl = new Vertex({ -0.5 + TILE_BEVEL, 0.0f, +0.5 }, { 0.0f, 1.0f });

        Mesh* wall = shapeFactory->newQuad(tileMat, NULL, NO_TEXTURE, bl, br, tr, tl);
        addRenderable(wall);
        for (int i = 1; i < 4; i++){
            wall = new Mesh(*wall);
            wall->setRotation(Y_AXIS, i * 90);
            addRenderable(wall);
        }
        
        
    //***************************** verticalEdges ************************************

        tl = new Vertex({ +0.5 - TILE_BEVEL, TILE_HEIGHT - TILE_BEVEL, +0.5 }, { 1.0f, 1.0f });
        bl = new Vertex({ +0.5 - TILE_BEVEL, 0.0f, +0.5 }, { 1.0f, 1.0f });        
        tr = new Vertex({ +0.5, TILE_HEIGHT - TILE_BEVEL, +0.5 - TILE_BEVEL }, { 0.0f, 1.0f });
        br = new Vertex({ +0.5, 0.0f, +0.5 - TILE_BEVEL }, { 0.0f, 1.0f });

        Mesh* vEdge = shapeFactory->newQuad(tileMat, NULL, NO_TEXTURE, bl, br, tr, tl);
        addRenderable(vEdge);
        for (int i = 1; i < 4; i++){
            vEdge = new Mesh(*vEdge);
            vEdge->setRotation(Y_AXIS, i * 90);
            addRenderable(vEdge);
        }



        //***************************** corners ************************************

        Vertex* t = new Vertex({ +0.5 - TILE_BEVEL, TILE_HEIGHT, +0.5 - TILE_BEVEL }, { 1.0f, 1.0f });
        Vertex* l = new Vertex({ +0.5 - TILE_BEVEL, TILE_HEIGHT - TILE_BEVEL, +0.5 }, { 1.0f, 1.0f });
        Vertex* r = new Vertex({ +0.5, TILE_HEIGHT - TILE_BEVEL, +0.5 - TILE_BEVEL }, { 0.0f, 1.0f });
    
        Mesh* corner = shapeFactory->newTriangle(tileMat, NULL, NO_TEXTURE, l, r, t);
        //long logT = clock();  
        addRenderable(corner);
        for (int i = 1; i < 4; i++){
            corner = new Mesh(*corner);
            corner->setRotation(Y_AXIS, i * 90);
            addRenderable(corner);
        }
        //if (clock() - logT != 0) { std::cout << "code needs " << clock() - logT << "ms\n"; }        
}


void Tile::setTransparency(GLfloat transparency){   //transparency: 0.0 - 1.0
    setColor({ -1, -1, -1, transparency });
}


Tile::~Tile(){
}


void Tile::setPosition(Coord2D position){
	this->position = position;
	setTranslation(-0.5 + position.x / boardSize.x + (1 / boardSize.x) / 2, 0, -0.5 + position.y / boardSize.y + (1 / boardSize.y) / 2);
	setScaling(1.0 / boardSize.x, TILE_HEIGHT, 1.0 / boardSize.y);
}

Coord2D Tile::getPosition()
{
	return this->position;
}

void Tile::setValue(int value){
    float fontSize = 0.7;
    if (value >= 16192){
        fontSize = 0.30;
    }
    else
    if (value >= 1024){
        fontSize = 0.35;
    }
    else
    if (value >= 128){
        fontSize = 0.45;
    }
    else
    if (value >= 16){
        fontSize = 0.6;
    }
    this->value = value;

    Color tileColor = Tile::getTileColor(value);
    Color labelColor = Tile::getLabelColor(value);
    Material* tileMat = new Material(TILE_MATERIAL);

    //tileMaterial an tileColor anpassen:
    tileMat->ambient[0] *= tileColor.r;
    tileMat->ambient[1] *= tileColor.g;
    tileMat->ambient[2] *= tileColor.b;

    setMaterial(FRONT, tileMat);
    


    char txt[32];
    sprintf(txt, "%d", value);
    label->setText(txt);
    label->setFontSize(fontSize);
    label->setTranslation(-label->getTextWidth() / 2, TILE_HEIGHT + 0.3, fontSize/2);


   

    
}

int Tile::getValue(){
    return value;
}


Color Tile::getTileColor(int value){
    switch (value){
        case     2: return{ 0.9, 0.9, 0.7, 1.0 };
        case     4: return{ 0.9, 0.8, 0.5, 1.0 };
        case     8: return{ 0.9, 0.7, 0.2, 1.0 };
        case    16: return{ 0.9, 0.5, 0.15, 1.0 };
        case    32: return{ 0.9, 0.2, 0.1, 1.0 };
        case    64: return{ 1.0, 0.0, 0.0, 1.0 };
        case   128: return{ 1.0, 0.0, 0.2, 1.0 };
        case   256: return{ 1.0, 0.1, 0.4, 1.0 };
        case   512: return{ 0.8, 0.2, 0.6, 1.0 };
        case  1024: return{ 0.7, 0.2, 0.8, 1.0 };        
        case  2048: return{ 0.5, 0.2, 0.8, 1.0 };
        case  4096: return{ 0.2, 0.0, 0.9, 1.0 };
        case  8192: return{ 0.0, 0.0, 0.9, 1.0 };
        case 16384: return{ 0.1, 0.3, 0.8, 1.0 };
        case 32768: return{ 0.2, 0.6, 0.6, 1.0 };
        case 65536: return{ 0.1, 0.6, 0.3, 1.0 };   //Lager values are not possible
        default: return COLOR_BLACK;
    }    
}

Color Tile::getLabelColor(int value){
    switch (value){
    case     2: return{ 0.45, 0.45, 0.45, 1.0 };
    case     4: return{ 0.55, 0.55, 0.55, 1.0 };
    case     8: return{ 0.7, 0.7, 0.7, 1.0 };
    case    16: return{ 1.0, 0.95, 0.8, 1.0 };
    case    32: return{ 1.0, 0.95, 0.78, 1.0 };
    case    64: return{ 1.0, 0.9, 0.9, 1.0 };
    case   128: return{ 1.0, 0.88, 0.95, 1.0 };
    case   256: return{ 1.0, 0.82, 0.9, 1.0 };
    case   512: return{ 1.0, 0.85, 0.95, 1.0 };
    case  1024: return{ 0.95, 0.85, 1.0, 1.0 };
    case  2048: return{ 0.95, 0.85, 1.0, 1.0 };
    case  4096: return{ 0.92, 0.85, 1.0, 1.0 };
    case  8192: return{ 0.98, 0.86, 1.0, 1.0 };
    case 16384: return{ 0.85, 0.9, 1.0, 1.0 };
    case 32768: return{ 0.9, 1.0, 0.9, 1.0 };
    case 65536: return{ 0.95, 1.0, 0.95, 1.0 };
    default: return COLOR_WHITE;
    }
}
