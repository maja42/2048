#pragma once

#include <list>
#include <iostream>

#include "Renderable.h"
#include "Color.h"


class Composite : public Renderable
{
private:
    std::list<Renderable*> childs;                  //list, because we need to remove Elements from the middle

public:
    Composite();
    virtual ~Composite();

    void addRenderable(Renderable* renderable);
    void addRenderableFront(Renderable* renderable);
    void removeRenderable(Renderable* renderable);
    void clearRenderables();

    void setColor(Color color);
    void setMaterial(Face face, Material* newMaterial);

    std::list<Renderable*> const* getRenderables() const;                                        //Returns all Childs
};

