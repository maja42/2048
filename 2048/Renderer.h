#pragma once
#define _CRT_SECURE_NO_DEPRECATE

#include <stdlib.h>
#include <list>
#include <stack>
#include <iostream>

#include "Composite.h"
#include "Mesh.h"
#include "LightSource.h"
#include "Tga.h"
#include "config.h"
#include "time.h"

#include "glut.h"
//#include <GL/gl.h>  
//#include <GL/glu.h>  


class Renderer
{
private:
    int windowID;                                   //The target-Window, where the renderer operates
   
    Composite overlay;                              //2D-Overlay
    Composite world3D;                              //3D-World  
   
    //Lightning (3D only):
        std::list<LightSource*> lightsources;           //LightSources for the 3D-World
        Material defaultMaterial;                       //default Lightning-values for an object
        void setMaterial(const Material *front, const Material *back);      //performs all lightning-operations for an object

    //Textures:      
        static GLint boundTexture;                      //-1: texturing disabled; otherwise: bound texture
        void prepareTexture(GLint texture);             //Enables or disables texturing (if it isn't already); if enabling: binds the texture (if it isn't bound already)

    //Helper:
        void renderComposite(Composite *comp);          //renders all Objects within a composition
        void renderMesh(Mesh *mesh);                    //renders this single Mesh
        void performTransformations(std::stack<const Transformation*> transformations);     //Performs all transformations
        void renderLightSources();


    long long renderCounter;
public:
    Renderer(int windowID);
    virtual ~Renderer();

    void addLightSource(LightSource* lightSource);
    void removeLightSource(LightSource* lightSource);

    Composite* get3DWorld();                                    //Returns a pointer to the whole world!
    Composite* get2DOverlay();                                  //Returns a pointer to the 2D Overlay
    void renderAll(Coord2D windowSize, Coord2D coordSize);      //Fenstergr��e, Gr��e des Zeichnungsraumes

    long long getRenderedMeshes();
};
