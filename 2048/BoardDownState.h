#pragma once

#include "IBoardRotationState.h"
#include "Board.h"

class BoardDownState : public IBoardRotationState
{
private:
	GLfloat _RotationAngle;
	short _Sign;

	TileField *_NewTileFields;
	bool _CalculatedNewFields;

public:
	BoardDownState();
	virtual ~BoardDownState();

    void UpdateLogic(Board* context, float elapsedTime);

private:
	void InitNewTileFields(int length);
	void OverrideTileFields(TileField *fields, int length);
    void AnimateBoard(Board *context, float elapsedTime);
    void AnimateTiles(Board *context, float elapsedTime);
	void CalcNewFields(Board *context);
	bool SetTileRecursive(Board *context, Tile *tile, int x, int y);
};