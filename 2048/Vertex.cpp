#include "Vertex.h"




Vertex::Vertex(Coord3D position, Coord2D textureCoord, Color color){
    this->position = position;
    
    this->textureCoord = textureCoord;
    this->color = color;
}



Vertex::~Vertex(){    
}

void Vertex::setPosition(Coord3D position){
    this->position = position;
}

Coord3D Vertex::getPosition(){
    return position;
}


void Vertex::setColor(Color color){
    if (color.r >= 0){
        this->color.r = color.r;
    }
    if (color.g >= 0){
        this->color.g = color.g;
    }
    if (color.b >= 0){
        this->color.b = color.b;
    }
    if (color.a >= 0){
        this->color.a = color.a;
    }
}

Color Vertex::getColor(){
    return color;
}

void Vertex::setTextureCoordinates(Coord2D textureCoord){
    this->textureCoord = textureCoord;
}

Coord2D Vertex::getTextureCoordinates(){
    return this->textureCoord;
}