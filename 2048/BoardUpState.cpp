#include "BoardUpState.h"

//Constructor
BoardUpState::BoardUpState()
{
	this->_RotationAngle = 0.0f;
	this->_Sign = -1;

	this->_NewTileFields = NULL;
	this->_CalculatedNewFields = false;
}

BoardUpState::~BoardUpState()
{
	delete[](this->_NewTileFields);
}


//IBoardRotationState
void BoardUpState::UpdateLogic(Board* context, float elapsedTime)
{
	if (!this->_CalculatedNewFields)
	{
		CalcNewFields(context);
	}
    AnimateTiles(context, elapsedTime);
    AnimateBoard(context, elapsedTime);
}


void BoardUpState::InitNewTileFields(int length)
{
	for (int i = 0; i < length; ++i)
	{
		this->_NewTileFields[i].first = NULL;
		this->_NewTileFields[i].second = NULL;
	}
}

void BoardUpState::OverrideTileFields(TileField *fields, int length)
{
	for (int i = 0; i < length; ++i)
	{
		fields[i] = this->_NewTileFields[i];
	}
}

void BoardUpState::AnimateBoard(Board *context, float elapsedTime)
{
    this->_RotationAngle += (this->_Sign * context->getRotationInc() * elapsedTime / 100.0f);
	if (this->_RotationAngle < (-context->getMaxRotation()))
	{
		this->_Sign = 1;
	}
	context->setRotation(X_AXIS, this->_RotationAngle);

	if (this->_RotationAngle >= 0.0f)
	{
		OverrideTileFields(context->getTileFields(), context->getTilesLength());
		context->setRotation(X_AXIS, 0.0f);
		context->setToInitialState();
		delete(this);
	}
}

void BoardUpState::AnimateTiles(Board *context, float elapsedTime)
{
	TileField *tileFields = context->getTileFields();
	Coord2D tileDimensions = context->getTileDimensions();

	for (int y = 0; y < (int)tileDimensions.y; ++y)
	{
		for (int x = 0; x < (int)tileDimensions.x; ++x)
		{
			Tile *currentTile = tileFields[context->getTileIndex(x, y)].first;
			if (currentTile != NULL)
			{
				int upY = y;
				while (this->_NewTileFields[context->getTileIndex(x, upY)].first != currentTile &&
					   this->_NewTileFields[context->getTileIndex(x, upY)].second != currentTile)
				{
					upY--;
				}

				Coord2D pos = currentTile->getPosition();
                if (pos.y - context->getTileMoveInc().y * elapsedTime / 100.0f > upY)
				{
                    pos.y -= context->getTileMoveInc().y * elapsedTime / 100.0f;
				}
				else
				{
					pos.y = upY;
				}
				currentTile->setPosition(pos);
			}
		}
	}
}

void BoardUpState::CalcNewFields(Board *context)
{
	this->_NewTileFields = new TileField[context->getTilesLength()];
	InitNewTileFields(context->getTilesLength());

	TileField *oldTileFields = context->getTileFields();
	Coord2D tileArrayDims = context->getTileDimensions();

	for (int y = 0; y < (int)tileArrayDims.y; ++y)
	{
		for (int x = 0; x < (int)tileArrayDims.x; ++x)
		{
			SetTileRecursive(context, oldTileFields[context->getTileIndex(x, y)].first, x, y);
		}
	}
	this->_CalculatedNewFields = true;
}

bool BoardUpState::SetTileRecursive(Board *context, Tile *tile, int x, int y)
{
	if (y < 0 || tile == NULL)
	{
		return false;
	}

	TileField *currentField = &this->_NewTileFields[context->getTileIndex(x, y)];
	if (currentField->first == NULL)
	{
		if (!SetTileRecursive(context, tile, x, y - 1))
		{
			currentField->first = tile;

			Coord2D tilePos = tile->getPosition();
			if (tilePos.x != x || tilePos.y != y)
			{
				context->setTilesMoved(true);
			}
		}
		return true;
	}
	else if (currentField->second == NULL && currentField->first->getValue() == tile->getValue())
	{
		currentField->second = tile;
		Coord2D tilePos = tile->getPosition();
		if (tilePos.x != x || tilePos.y != y)
		{
			context->setTilesMoved(true);
		}
		return true;
	}
	return false;
}