#pragma once

#define _CRT_SECURE_NO_WARNINGS


#include "Composite.h"
#include "Coord.h"
#include "config.h"
#include "Tga.h"
#include "Mesh.h"
#include "ShapeFactory.h"
#include "Text.h"

class Tile : public Composite{
    static Coord2D boardSize;

    Coord2D position;
    int value;
    Text* label;

    static Color getTileColor(int value);
    static Color Tile::getLabelColor(int value);
public:
	Tile(int value, Coord2D position);
    virtual ~Tile();
    void setPosition(Coord2D position);
	Coord2D getPosition();
	void setValue(int value);
    int getValue();
    void setTransparency(GLfloat transparency); //transparency: 0.0 - 1.0
};
struct TileField {
	Tile* first;
	Tile* second;
};