#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include "Coord.h"

#define MAIN_WINDOW_WIDTH 800
#define MAIN_WINDOW_HEIGHT 600
#define MAIN_WINDOW_TITLE "2048"


extern Coord2D BOARD_SIZE;
//#define BOARD_SIZE {4,4}        //Number of tiles in x- and y-direction









//#define TILE_SIZE 2             //The size of a single tile
#define BOARD_HEIGHT 0.25        //Die H�he des Boards
#define TILE_HEIGHT 0.9			//Die H�he des Steines relativ zur Boardh�he
#define TILE_BEVEL 0.15          //Breite + H�he der Tile-Abschr�gung in %




#define SCOREBOX_PADDING 20         //Abstand der Scorebox zum Fensterrand
#define SCOREBOX_SIZE {150, 90}

#define CAMBOX_PADDING {20, 130}    //Abstand der Cambox zum Fensterrand
#define CAMBOX_SIZE {150,50}

#define ENDBOX_SIZE {550, 200}


#define ENDBOX_FADEID_DURATION 3000
#define BOX_FADEID_DURATION 1500    //Dauer, wie lange das Einblenden der Scorebox, Cambox und Endbox dauert
#define SCORE_EFFECT_DURATION 500   //Dauer der Score-Up-Effekte in millisekunden


#define CONFIG_PATH "data/config.txt"

#define DEFAULT_MATERIAL { \
	/*wei�es gl�nzen*/			{ 1.0, 1.0, 1.0, 1.0 },		\
	/*Glanzst�rke*/				5.0,						\
	/*diffuse*/					{ 1.0, 1.0, 1.0, 1.0 },		\
	/*umgebungslicht*/			{ 1.0, 1.0, 1.0, 1.0 },		\
	/*leuchtkraft objekt*/		{ 0.0, 0.0, 0.0, 1.0 }		\
}

//Materials:
#define BOARD_FLOOR_MATERIAL { \
	/*specular*/				{ 0.4, 0.4, 0.4, 1.0 },		\
	/*shininess*/				0.25,						\
	/*diffuse*/					{ 0.7, 0.7, 0.7, 1.0 },		\
	/*ambient*/					{ 0.2, 0.2, 0.2, 1.0 },		\
	/*emitted*/					{ 0.0, 0.0, 0.0, 1.0 }		\
}
//0.161, 0.271, 0.384
#define BOARD_WALL_MATERIAL { \
	/*specular*/				{ 0.4, 0.4, 0.4, 1.0 },		\
	/*shininess*/				25.0,						\
	/*diffuse*/					{ 0.1, 0.1, 0.1, 1.0 },		\
	/*ambient*/					{0.12, 0.15, 0.20, 1.0 },	\
	/*emitted*/					{ 0.0, 0.0, 0.0, 1.0 }		\
}

//#define TILE_MATERIAL { \
//	/*specular*/				{ 0.4, 0.4, 0.4, 0.4 },		\
//	/*shininess*/				5.0,						\
//	/*diffuse*/					{ 0.5, 0.5, 0.5, 0.4 },		\
//	/*ambient*/					{ 0.2, 0.2, 0.2, 0.4 },		\
//	/*emitted*/					{ 0.2, 0.2, 0.0, 0.6 }		\
//}


#define TILE_MATERIAL { \
    /*specular*/				{ 0.4, 0.4, 0.4, 1.0 },		\
    /*shininess*/				25.0,						\
    /*diffuse*/					{ 0.5, 0.5, 0.5, 1.0 },		\
    /*ambient*/					{ 0.2, 0.2, 0.2, 1.0 },		\
    /*emitted*/					{ 0.0, 0.0, 0.0, 1.0 }		\
}



struct Config
{
	struct Coord2D boardSize;
};

Config loadConfig();