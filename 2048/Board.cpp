#include "Board.h"

//size: amount of tiles in x- and y-direction
Board::Board(IGameWindow *gameWindow, IBoardRotationState *initialState, const Coord2D size) :
ROTATION_INC(15.0f), MAX_ROTATION(20.0f), TILE_MOVE_INC({ size.x / 1.4f, size.y / 1.4f }), TILE_TRANSLATION_INC(1.5f)
{
	this->size = size;
	boardBorder = new BoardBorder(size);
	this->gameWindow = gameWindow;

	this->tilesLength = (int)(size.x * size.y);
	this->tileFields = new TileField[this->tilesLength];
	this->tilesMoved = true;
	this->rotationState = this->initialState = initialState;

    addRenderable(boardBorder);
	setScaling(size.x, BOARD_HEIGHT, size.y);

	initTiles();

    
    /*
	this->tileFields[0].first = new Tile(1024, { 0, 0 });
	addRenderable(this->tileFields[0].first);

	this->tileFields[1].first = new Tile(1024, { 1, 0 });
	addRenderable(this->tileFields[1].first);*/
}

Board::~Board(){
	delete(boardBorder);
	delete(this->initialState);

	for (int i = 0; i < this->tilesLength; ++i)
	{
		delete(this->tileFields[i].first);
		delete(this->tileFields[i].second);
	}
	delete[](this->tileFields);
}

void Board::initTiles()
{
	for (int i = 0; i < this->tilesLength; ++i)
	{
		this->tileFields[i].first = NULL;
		this->tileFields[i].second = NULL;
	}
}



void Board::update(float elapsedTime)
{
    this->rotationState->UpdateLogic(this, elapsedTime);
}

void Board::setState(IBoardRotationState *state)
{
	if (state != NULL)
	{
		this->rotationState = state;
	}
}

void Board::setToInitialState()
{
	this->rotationState = this->initialState;
}

GLfloat Board::getRotationInc()
{
	return this->ROTATION_INC;
}

GLfloat Board::getMaxRotation()
{
	return this->MAX_ROTATION;
}

int Board::getTileIndex(int x, int y)
{
	if (x < 0){ x = 0; }
	if (x >= (int)this->size.x){ x = (int)this->size.x - 1; }
	if (y < 0){ y = 0; }
	if (y >= (int)this->size.y){ y = (int)this->size.y - 1; }

	return (y * (int)this->size.x + x);
}

Coord2D Board::getTileMoveInc()
{
	return this->TILE_MOVE_INC;
}

GLfloat Board::getTileTranslationInc()
{
	return this->TILE_TRANSLATION_INC;
}

int Board::getTilesLength()
{
	return this->tilesLength;
}

Coord2D Board::getTileDimensions()
{
	return this->size;
}

TileField* Board::getTileFields()
{
	return this->tileFields;
}

bool Board::getTilesMoved()
{
	return this->tilesMoved;
}

void Board::setTilesMoved(bool value)
{
	this->tilesMoved = value;
}

void Board::addScore(int amount)
{
	this->gameWindow->addScore(amount);
}

void Board::onGameWin()
{
	this->gameWindow->onGameWin();
}

void Board::onGameLoose()
{
	this->gameWindow->onGameLoose();
}

void Board::removeAllTiles()
{
	for (int i = 0; i < this->tilesLength; ++i)
	{
		if (this->tileFields[i].first != NULL)
		{
			removeRenderable(this->tileFields[i].first);
		}
		if (this->tileFields[i].second != NULL)
		{
			removeRenderable(this->tileFields[i].second);
		}
		delete(this->tileFields[i].first);
		delete(this->tileFields[i].second);
	}
	initTiles();
	this->tilesMoved = true;
}