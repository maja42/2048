#pragma once

#include "Composite.h"
#include "Coord.h"
#include "BoardBorder.h"
#include "Tile.h"
#include "GameComponent.h"

class Skybox : public Composite
{
private:
	Tga* skyBoxTexture;
	float scale;
	static Coord2D boardSize;

public:
	Skybox();
	~Skybox();
};

