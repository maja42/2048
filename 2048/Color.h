#pragma once

#include "glut.h"
//#include <GL/gl.h>  
//#include <GL/glu.h> 

struct Color{
    GLfloat r;
    GLfloat g;
    GLfloat b;
    GLfloat a;
};

#define COLOR_WHITE {1,1,1,1}
#define COLOR_BLACK {0,0,0,1}

Color complementary(Color color);