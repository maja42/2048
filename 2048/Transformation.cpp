#include "Transformation.h"


Transformation translation(GLfloat x, GLfloat y, GLfloat z){
    return{ x, y, z, 0.0f, TRANSLATE_MATRIX };
}

Transformation scaling(GLfloat x, GLfloat y, GLfloat z){
    return{ x, y, z, 0.0f, SCALE_MATRIX };
}

Transformation rotation(Axis axis, GLfloat angle){
    switch (axis)
    {
    case X_AXIS:    return{ 1, 0, 0, angle, ROTATE_MATRIX };
    case Y_AXIS:    return{ 0, 1, 0, angle, ROTATE_MATRIX };
    case Z_AXIS:    return{ 0, 0, 1, angle, ROTATE_MATRIX };
    }

    return{};
}


bool operator == (const Transformation& a, const Transformation& b){
    return std::tie(a.x, a.y, a.z, a.alpha, a.type) == std::tie(b.x, b.y, b.z, b.alpha, b.type);
}

bool operator != (const Transformation& a, const Transformation& b){
    return std::tie(a.x, a.y, a.z, a.alpha, a.type) != std::tie(b.x, b.y, b.z, b.alpha, b.type);
}