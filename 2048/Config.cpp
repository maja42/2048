#include "config.h"


//Coord2D BOARD_SIZE = { 4, 4 };        //Number of tiles in x- and y-direction


Config loadConfig(){
	struct Config Board;
	int x, y;
	FILE * pFile;

	pFile = fopen(CONFIG_PATH, "r+");
	if (NULL == pFile) {
		perror("ConfigFile fuer Boardsize nicht gefunden -> CONFIG_PATH pruefen");
		Board.boardSize.x = 4;
		Board.boardSize.y = 4;
	}

    if (fscanf(pFile, "%dx%d", &x, &y) != 2){
        Board.boardSize.x = 4;
        Board.boardSize.y = 4;
    }else
    if(x<0 || y<0 || x>20 || y>20){
        Board.boardSize.x = 4;
        Board.boardSize.y = 4;
    }
    else{
        Board.boardSize.x = x;
        Board.boardSize.y = y;
    }
	fclose(pFile);

	
    
	return Board;
}

Config config = loadConfig();
Coord2D BOARD_SIZE = config.boardSize;