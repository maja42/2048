#include "Color.h"

Color complementary(Color color){
    return{ 1.0f - color.r, 1.0f - color.g, 1.0f - color.b, color.a };
}