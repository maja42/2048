#include "main.h"


int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    srand(time(NULL));   


    try{
         new GameWindow({ 0, 0 }, { 800, 600 }, "2048");
    }
    catch (std::exception e){
        std::cout << "Error during initialising - an exception has been thrown" << std::endl << e.what() << std::endl;
        system("PAUSE");
        exit(0);
    }
    

    glutMainLoop();                       //Glut has now controll about everything

    return 0;
}