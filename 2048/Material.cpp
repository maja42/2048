#include "Material.h"

void setTransparency(Material *material, GLfloat transparency){     //transparency: 0.0 - 1.0
    material->specular[3] = transparency;
    material->diffuse[3] = transparency;
    material->ambient[3] = transparency;
}