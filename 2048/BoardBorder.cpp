#include "BoardBorder.h"
#include "ShapeFactory.h"
#include "Tga.h"

BoardBorder::BoardBorder(Coord2D size){
    this->size = size;

  
	ShapeFactory* shapeFactory = ShapeFactory::getInstance();
  

	Material* floorMat = new Material(BOARD_FLOOR_MATERIAL);
	Material* wallMat = new Material(BOARD_WALL_MATERIAL);
	Material* tileMat = new Material(TILE_MATERIAL);

    floorTexture = new Tga("data/boardFloor.tga");


	//***************************** bottom ************************************


	Vertex* v1 = new Vertex({ -0.5, 0, +0.5 }, { 0.0f, 0.0f });
	Vertex* v2 = new Vertex({ +0.5, 0, +0.5 }, { 6.0f, 0.0f });
	Vertex* v3 = new Vertex({ +0.5, 0, -0.5 }, { 6.0f,12.0f });
    Vertex* v4 = new Vertex({ -0.5, 0, -0.5 }, { 0.0f, 12.0f });
    Mesh* floor = shapeFactory->newQuad(floorMat, NULL, floorTexture->getTextureId(), v1, v2, v3, v4);
    addRenderable(floor);


	//***************************** innerWall ************************************


	Vertex* tl = new Vertex({ -0.5, 0, -0.5 }, { 0.0f, 0.0f });
	Vertex* tr = new Vertex({ -0.5, 1.0, -0.5 }, { 1.0f, 0.0f });
	Vertex* br = new Vertex({ +0.5, 1.0, -0.5 }, { 1.0f, 12.0f });
	Vertex* bl = new Vertex({ +0.5, 0, -0.5 }, { 0.0f, 12.0f });
	
	//bl, br, tr, tl

	Mesh* innerWall = shapeFactory->newQuad(wallMat, wallMat, NO_TEXTURE, tl, bl, br, tr);
	addRenderable(innerWall);
	for (int i = 1; i < 4; i++){
		innerWall = new Mesh(*innerWall);
		innerWall->setRotation(Y_AXIS, i * 90);
		addRenderable(innerWall);
	}

	//******************** outerWall *****************************

	tl = new Vertex({ -0.5, 0, -0.55 }, { 0.0f, 0.0f });
	tr = new Vertex({ -0.5, 0.5, -0.55 }, { 1.0f, 0.0f });
	br = new Vertex({ +0.5, 0.5, -0.55 }, { 1.0f, 1.0f });
	bl = new Vertex({ +0.5, 0, -0.55 }, { 0.0f, 1.0f });


	Mesh* outerWall = shapeFactory->newQuad(wallMat, wallMat, NO_TEXTURE, bl, br, tr, tl);
	addRenderable(outerWall);
	for (int i = 1; i < 4; i++){
		outerWall = new Mesh(*outerWall);
		outerWall->setRotation(Y_AXIS, i * 90);
		addRenderable(outerWall);
	}

	//******************** topWall *****************************
	
	tl = new Vertex({ -0.5, 0.5, -0.55 }, { 0.0f, 0.0f });
	tr = new Vertex({ +0.5, 0.5, -0.55 }, { 12.0f, 0.0f });
	br = new Vertex({ +0.5, 1.0, -0.5 }, { 12.0f, 1.0f });
	bl = new Vertex({ -0.5, 1.0, -0.5 }, { 0.0f, 1.0f });

	Mesh* topWall = shapeFactory->newQuad(wallMat, wallMat, NO_TEXTURE, bl, br, tr, tl);
	addRenderable(topWall);
	for (int i = 1; i < 4; i++){
		topWall = new Mesh(*topWall);
		topWall->setRotation(Y_AXIS, i * 90);
		addRenderable(topWall);
	}

	//******************** edges *****************************

	tl = new Vertex({ -0.5, 0.0, -0.55 }, { 0.0f, 0.0f });
	tr = new Vertex({ -0.5, 0.5, -0.55 }, { 1.0f, 0.0f });
	br = new Vertex({ -0.55, 0.5, -0.5 }, { 1.0f, 1.0f });
	bl = new Vertex({ -0.55, 0.0, -0.5 }, { 0.0f, 1.0f });

	Mesh* edge = shapeFactory->newQuad(wallMat, wallMat, NO_TEXTURE, bl, br, tr, tl);
	addRenderable(edge);
	for (int i = 1; i < 4; i++){
		edge = new Mesh(*edge);
		edge->setRotation(Y_AXIS, i * 90);
		addRenderable(edge);
	}

	//***************************** corners ************************************
	
	Vertex* t = new Vertex({ -0.5, 1.0, -0.5 }, { 1.0f, 1.0f });
	Vertex* l = new Vertex({ -0.5, 0.5, -0.55 }, { 1.0f, 1.0f });
	Vertex* r = new Vertex({ -0.55, 0.5, -0.5 }, { 0.0f, 1.0f });


	Mesh* corner = shapeFactory->newTriangle(wallMat, wallMat, NO_TEXTURE, t, l, r);
	addRenderable(corner);
	for (int i = 1; i < 4; i++){
		corner = new Mesh(*corner);
		corner->setRotation(Y_AXIS, i * 90);
		addRenderable(corner);
	}
}


BoardBorder::~BoardBorder(){
    delete floorTexture;
}
