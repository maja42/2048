#pragma once

#include <stdlib.h>
#include <list>
#include <time.h>

#include "Coord.h"
#include "GlutWindow.h"
#include "Keyboard.h"

class GlutWindow;                                                   //We need a class forward declaration here


//SINGLETON
class WindowManager
{
private:
    WindowManager();
    virtual ~WindowManager();
  
	static WindowManager* singleton;
    std::list<GlutWindow*> windows;                                //List with all visible Windows

	long previousFrameTime;
	short* elapsedTimeHistory;
	static const USHORT ELAPSED_TIME_HISTORTY_LENGTH = 15;
	USHORT elapsedTimeHistoryIndex;

	float averageFrameTime;

    //Window-independent Glut-Functions:
    static void resize(int width, int height);                      //Wird aufgerufen, wenn irgendein Fenster resized wird
    static void display();                                          //Wird beim neu zeichnen von einem beliebigen Fenster aufgerufen
	static void staticFpsTimer(int value);
	void fpsTimer(int value);
	void calcAverage();

    static void mouseFunc(int button, int state, int x, int y);     //Wird bei einem Tastendruck der Maus innerhalb irgendeines Fensters aufgerufen
    static void mouseMotionFunc(int x, int y);                      //Wird bei der Mausbewegung innerhalb irgendeines Fensters aufgerufen, wenn eine oder mehr Tasten gedr�ckt sind
    static void mousePassiveMotionFunc(int x, int y);               //Wird bei der Mausbewegung innerhalb des Fensters aufgerufen, wenn keine Taste gedr�ckt ist
    static void mouseEntryFunc(int state);                          //Wird aufgerufen, wenn ein Fenster den Fokus verliert oder bekommt

    GlutWindow* getWindowObject(int windowID);                      //Returns the window-Object of the given ID
    static Coord2D convertWindowCoordinates(GlutWindow* targetWindow, int x, int y);     //Converts Window-Coordinates into correct Matrix-Coordinates
public:
    static WindowManager* getInstance();                            //Gibt die Singleton-Instanz zur�ck (Lazy Initialisierung)

    void registerWindow(GlutWindow* newWindow);
    void deregisterWindow(GlutWindow* oldWindow);
     
    void renderAllWindows();                                        //Renders all Windows
};

