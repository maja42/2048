#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <list>

#include "GameComponent.h"
#include "Composite.h"
#include "Mesh.h"
#include "BorderBox2D.h"
#include "Text.h"
#include "config.h"
#include "Keyboard.h"


struct ScoreEffect{
    Text* output;
    float progress;
};

class ScoreBox : public Composite, public GameComponent{
    Coord2D boxSize;

    int score;
    Text* scoreText;

    std::list<ScoreEffect*> scoreEffects;

    float fadeInProgress;

    void updateScore();
public:
    ScoreBox();
    virtual ~ScoreBox();

    int getScore();
    void addPoints(int points);
    void update(float elapsedTime);
};

