#include "ScoreBox.h"


ScoreBox::ScoreBox()
{
    boxSize = SCOREBOX_SIZE;

    fadeInProgress = 0;
    score = 0;

    BorderBox2D* background = new BorderBox2D({ MAIN_WINDOW_WIDTH - boxSize.x - SCOREBOX_PADDING, MAIN_WINDOW_HEIGHT - boxSize.y - SCOREBOX_PADDING }, boxSize, 15, BLACK_BOX);
    Text* titleText = new Text("Score:", 30, { 1.0, 0.0, 0.0, 0.0 });
    scoreText = new Text("0", 45, { 1.0, 1.0, 0.0, 0.0 });

    titleText->setTranslation(MAIN_WINDOW_WIDTH - SCOREBOX_PADDING - boxSize.x / 2 - titleText->getTextWidth() / 2, MAIN_WINDOW_HEIGHT - SCOREBOX_PADDING - 40, 0);
    updateScore();

    addRenderable(background);
    addRenderableFront(titleText);
    addRenderableFront(scoreText);
    
}


ScoreBox::~ScoreBox(){
    for (std::list<ScoreEffect*>::iterator iter = scoreEffects.begin(); iter != scoreEffects.end(); iter++){
        removeRenderable((*iter)->output);
        delete (*iter)->output;
        delete (*iter);
    }
    scoreEffects.clear();
}

void ScoreBox::updateScore(){
    char str[32];
    sprintf(str, "%d", score);
    scoreText->setText(str);
    scoreText->setTranslation(MAIN_WINDOW_WIDTH - SCOREBOX_PADDING - boxSize.x / 2 - scoreText->getTextWidth() / 2, MAIN_WINDOW_HEIGHT - SCOREBOX_PADDING - 79, 0);
}

int ScoreBox::getScore(){
    return score;
}

void ScoreBox::addPoints(int points){
    score += points;
    updateScore();
    
    //Add an update-Effect:
    char str[32];
    sprintf(str, "+%d", points);

    Text* output = new Text(str, 40, { 0.5, 0.5, 1.0, 1.0 });
    output->setTranslation(MAIN_WINDOW_WIDTH - SCOREBOX_PADDING - boxSize.x / 2 + scoreText->getTextWidth() / 2 - output->getTextWidth(), MAIN_WINDOW_HEIGHT - SCOREBOX_PADDING - 85, 0);
    addRenderableFront(output);
    ScoreEffect* effect = new ScoreEffect({
        output,
        0
    });    
    scoreEffects.push_front(effect);    
}

void ScoreBox::update(float elapsedTime){
    for (std::list<ScoreEffect*>::iterator iter = scoreEffects.begin(); iter != scoreEffects.end();){
        float* progress = &((*iter)->progress);
        *progress += (elapsedTime / SCORE_EFFECT_DURATION * 100);
        if (*progress > 100){ 
            removeRenderable((*iter)->output);
            delete (*iter)->output;
            delete(*iter);
            scoreEffects.erase(iter++);            
        }
        else{
            (*iter)->output->setColor({ -1.0, -1.0, -1.0, 1.0 - (*progress / 100.0f) });
            (*iter)->output->setTranslation(MAIN_WINDOW_WIDTH - SCOREBOX_PADDING - boxSize.x / 2 + scoreText->getTextWidth() / 2 - (*iter)->output->getTextWidth(), MAIN_WINDOW_HEIGHT - SCOREBOX_PADDING - 85 - (*progress) / 2, 0);
            ++iter;
        }        
        //std::cout << elapsedTime << "ms\n";
    }


    if (fadeInProgress < 100){
        fadeInProgress += (elapsedTime / BOX_FADEID_DURATION * 100);
        if (fadeInProgress > 100){ fadeInProgress = 100; }
        this->setColor({ -1, -1, -1, fadeInProgress / 100.0f });    //Only change the alpha-channel
    }
    


    /*if(Keyboard::GetInstance()->IsKeyPressed(AK_Space)){
        addPoints(42);
    }*/
    
}