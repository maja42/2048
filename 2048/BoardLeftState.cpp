#include "BoardLeftState.h"

//Constructor
BoardLeftState::BoardLeftState()
{
	this->_RotationAngle = 0.0f;
	this->_Sign = 1;

	this->_NewTileFields = NULL;
	this->_CalculatedNewFields = false;
}

BoardLeftState::~BoardLeftState()
{
	delete[](this->_NewTileFields);
}


//IBoardRotationState
void BoardLeftState::UpdateLogic(Board *context, float elapsedTime)
{
	if (!this->_CalculatedNewFields)
	{
		CalcNewFields(context);
	}
    AnimateTiles(context, elapsedTime);
    AnimateBoard(context, elapsedTime);
}


void BoardLeftState::InitNewTileFields(int length)
{
	for (int i = 0; i < length; ++i)
	{
		this->_NewTileFields[i].first = NULL;
		this->_NewTileFields[i].second = NULL;
	}
}

void BoardLeftState::OverrideTileFields(TileField *fields, int length)
{
	for (int i = 0; i < length; ++i)
	{
		fields[i] = this->_NewTileFields[i];
	}
}

void BoardLeftState::AnimateBoard(Board *context, float elapsedTime)
{
    this->_RotationAngle += (this->_Sign * context->getRotationInc() * elapsedTime / 100.0f);
	if (this->_RotationAngle > context->getMaxRotation())
	{
		this->_Sign = -1;
	}
	context->setRotation(Z_AXIS, this->_RotationAngle);

	if (this->_RotationAngle <= 0.0f)
	{
		OverrideTileFields(context->getTileFields(), context->getTilesLength());
		context->setRotation(Z_AXIS, 0.0f);
		context->setToInitialState();
		delete(this);
	}
}

void BoardLeftState::AnimateTiles(Board *context, float elapsedTime)
{
	TileField *tileFields = context->getTileFields();
	Coord2D tileDimensions = context->getTileDimensions();

	for (int x = 0; x < (int)tileDimensions.x; ++x)
	{
		for (int y = 0; y < (int)tileDimensions.y; ++y)
		{
			Tile *currentTile = tileFields[context->getTileIndex(x, y)].first;
			if (currentTile != NULL)
			{
				int leftX = x;
				while (this->_NewTileFields[context->getTileIndex(leftX, y)].first != currentTile &&
					   this->_NewTileFields[context->getTileIndex(leftX, y)].second != currentTile)
				{
					leftX--;
				}

				Coord2D pos = currentTile->getPosition();
                if (pos.x - context->getTileMoveInc().x * elapsedTime / 100.0f > leftX)
				{
                    pos.x -= context->getTileMoveInc().x * elapsedTime / 100.0f;
				}
				else
				{
					pos.x = leftX;
				}
				currentTile->setPosition(pos);
			}
		}
	}
}

void BoardLeftState::CalcNewFields(Board *context)
{
	this->_NewTileFields = new TileField[context->getTilesLength()];
	InitNewTileFields(context->getTilesLength());

	TileField *oldTileFields = context->getTileFields();
	Coord2D tileArrayDims = context->getTileDimensions();

	for (int x = 0; x < (int)tileArrayDims.x; ++x)
	{
		for (int y = 0; y < (int)tileArrayDims.y; ++y)
		{
			SetTileRecursive(context, oldTileFields[context->getTileIndex(x, y)].first, x, y);
		}
	}
	this->_CalculatedNewFields = true;
}

bool BoardLeftState::SetTileRecursive(Board *context, Tile *tile, int x, int y)
{
	if (x < 0 || tile == NULL)
	{
		return false;
	}

	TileField *currentField = &this->_NewTileFields[context->getTileIndex(x, y)];
	if (currentField->first == NULL)
	{
		if (!SetTileRecursive(context, tile, x - 1, y))
		{
			currentField->first = tile;

			Coord2D tilePos = tile->getPosition();
			if (tilePos.x != x || tilePos.y != y)
			{
				context->setTilesMoved(true);
			}
		}
		return true;
	}
	else if (currentField->second == NULL && currentField->first->getValue() == tile->getValue())
	{
		currentField->second = tile;
		Coord2D tilePos = tile->getPosition();
		if (tilePos.x != x || tilePos.y != y)
		{
			context->setTilesMoved(true);
		}
		return true;
	}
	return false;
}