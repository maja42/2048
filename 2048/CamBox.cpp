#include "CamBox.h"


CamBox::CamBox(int camNr)
{
    boxSize = CAMBOX_SIZE;
    Coord2D boxPadding = CAMBOX_PADDING;

    fadeInProgress = 0;
    this->camNr = camNr;

    camBox = new BorderBox2D({ MAIN_WINDOW_WIDTH - boxPadding.x - boxSize.x, MAIN_WINDOW_HEIGHT - boxPadding.y - boxSize.y }, CAMBOX_SIZE, 18, BLACK_BOX);
    camBox->setColor({ 0.5, 0.5, 1.0, 0.0 });
    
    Text* titleText = new Text("Cam:", 25, { 1.0, 0.0, 0.0, 0.0 });
    titleText->setTranslation(MAIN_WINDOW_WIDTH - boxPadding.x - boxSize.x + 8, MAIN_WINDOW_HEIGHT - boxPadding.y - boxSize.y / 2 - 12.5, 0);
        
    char str[32];
    sprintf(str, "%d", camNr);
    camText = new Text(str, 45, { 1.0, 1.0, 0.0, 0.0 });
    camText->setTranslation(MAIN_WINDOW_WIDTH - boxPadding.x - boxSize.x / 2 - camText->getTextWidth() / 2, MAIN_WINDOW_HEIGHT - boxPadding.y - boxSize.y / 2 - 22.5, 0);


    addRenderable(camBox);
    addRenderableFront(titleText);
    addRenderableFront(camText);
}


CamBox::~CamBox()
{
}

void CamBox::updateCamText(){

}

void CamBox::setCam(int camNr){
    this->camNr = camNr;
    char str[32];
    sprintf(str, "%d", camNr);
    camText->setText(str);
}

void CamBox::update(float elapsedTime){
    if (fadeInProgress < 100){
        fadeInProgress += (elapsedTime / BOX_FADEID_DURATION * 100);
        if (fadeInProgress > 100){ fadeInProgress = 100; }
        this->setColor({ -1, -1, -1, fadeInProgress / 100.0f });    //Only change the alpha-channel
    }
    //std::cout << elapsedTime << "ms\n";
}

