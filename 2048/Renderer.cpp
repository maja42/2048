#include "Renderer.h"

GLint Renderer::boundTexture = -1;      //No texture bound


Renderer::Renderer(int windowID_){
    windowID = windowID_;

	defaultMaterial = DEFAULT_MATERIAL;
}

Renderer::~Renderer(){
}


Composite* Renderer::get3DWorld(){
    return &world3D;
}


Composite* Renderer::get2DOverlay(){                                  //Returns a pointer to the 2D Overlay
    return &overlay;
}


void Renderer::renderAll(Coord2D windowSize, Coord2D coordSize){
    //long tLog = clock();
    //std::cout << "renderall" << std::endl; 
    glutSetWindow(windowID);
    glViewport(0, 0, windowSize.x, windowSize.y);                   //Zeichnungsbereich innerhalb des Fensters (auch mehrere m�glich)
    renderCounter = 0;

    //3D:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);             //Bildschirm jetzt komplett l�schen
    glEnable(GL_LIGHTING);	                                        //Beleuchtung aktivieren

    glMatrixMode(GL_PROJECTION);
        glLoadIdentity();    
        gluPerspective(45.0f, (GLfloat)windowSize.x / (GLfloat)windowSize.y, 0.1f, 30000.0f);   //Kamera-�ffnungswinkel, Seitenverh�ltnis, Genauigkeit (Elemente >= 0.1f Einheiten), Distance (Elemente m�ssen n�her als 100.0f sein)
    glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        renderLightSources();
           
        try{
            renderComposite(&world3D);
        }
        catch (std::exception e){
            std::cout << "Error during 3D-rendering - an exception has been thrown" << std::endl << e.what() << std::endl;
            system("PAUSE");
            exit(0);
        }
        
        //std::cout << "    Vorgang dauert " << clock() - tLog << "ms \n";

        //TEAPOT
        //Material* mat = new Material(TILE_MATERIAL);
        //setMaterial(mat, mat);
        //glutSolidTeapot(3);
        
       
  
    //2D: HUD-Overlay   
    glClear(GL_DEPTH_BUFFER_BIT);   //Tiefenbuffer l�schen, damit 3D-content nicht den 2D-Content �berlagern kann
    glDisable(GL_LIGHTING);	        //Beleuchtung f�r das HUD deaktivieren
    glMatrixMode(GL_PROJECTION);    
        glLoadIdentity();
        //I need those strange values, in order to snap the 2D-Overlay on the top & right borders
        glViewport(windowSize.x - coordSize.x, windowSize.y - coordSize.y, coordSize.x, coordSize.y);       //Zeichnungsbereich innerhalb des Fensters (auch mehrere m�glich)
        glOrtho(0, coordSize.x, 0, coordSize.y, 0, 1);                                                      //left/right/bottom/top / near/far;  We use the windowSize, so that the box-size always stays the same, and doesn't change the ratio/size when resizing
    glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    
        try{
            renderComposite(&overlay);
        }
        catch (std::exception e){
            std::cout << "Error during 2D-rendering - an exception has been thrown" << std::endl << e.what() << std::endl;
            system("PAUSE");
            exit(0);
        }        

    //SWAP!
    
    glutSwapBuffers();   
    
    //std::cout << "Rendered meshes: "<< renderCounter << "\n";    
}

long long Renderer::getRenderedMeshes(){
    return renderCounter;
}


void Renderer::setMaterial(const Material *front, const Material *back){      //performs all lightning-operations for an object
    const Material* ptr = front;
    Face face = FRONT;
    
    for(int i = 0; i<2; i++){
        
        //ptr = &mat;
        if (ptr == NULL){
            ptr = &defaultMaterial;
        }
        GLfloat shininess[] = { ptr->shininess };					

        glMaterialfv(face, GL_SPECULAR, ptr->specular);	
        glMaterialfv(face, GL_SHININESS, shininess);            
            
        glMaterialfv(face, GL_DIFFUSE, ptr->diffuse);
        glMaterialfv(face, GL_AMBIENT, ptr->ambient);            
        glMaterialfv(face, GL_EMISSION, ptr->emission);
        
        ptr = back;
        face = BACK;
    }    
}

void Renderer::renderLightSources(){  

    GLenum lightID;
    int i = 0;
    for (std::list<LightSource*>::const_iterator light = lightsources.begin(); light != lightsources.end(); light++){
        switch (i++){
            case 0: lightID = GL_LIGHT0; break;
            case 1: lightID = GL_LIGHT1; break;
            case 2: lightID = GL_LIGHT2; break;
            case 3: lightID = GL_LIGHT3; break;
            case 4: lightID = GL_LIGHT4; break;
            case 5: lightID = GL_LIGHT5; break;
            case 6: lightID = GL_LIGHT6; break;
            case 7: lightID = GL_LIGHT7; break;
            default: return;
        }        
                
        glLightfv(lightID, GL_AMBIENT, (*light)->ambient);		//
        glLightfv(lightID, GL_DIFFUSE, (*light)->diffuse);

        glLightfv(lightID, GL_POSITION, (*light)->position);    //Position der Lichtquelle
        
        glEnable(lightID);		//Lichtquelle einschalten (kann Objektabh�ngig ein- und ausgeschalten werden)

        //GLfloat c2 = 2;
        //glLightfv(lightID, GL_CONSTANT_ATTENUATION, 1);
        //glLightfv(lightID, GL_LINEAR_ATTENUATION, &c2);
        //glLightfv(lightID, GL_QUADRATIC_ATTENUATION, 3);


        //Can be uncommented, to see the lightsource
		//glPushMatrix();
		//glTranslatef((*light)->position[0], (*light)->position[1], (*light)->position[2]);
		//glutSolidSphere(0.5,16,16);
		//glPopMatrix();
		
    }
}


void Renderer::performTransformations(std::stack<const Transformation*> transformations){     //Performs all transformations
    while (!transformations.empty()){
        const Transformation* transform = transformations.top();
        transformations.pop();

        switch (transform->type){
            case TRANSLATE_MATRIX:      glTranslatef(transform->x, transform->y, transform->z);                 break;  //Verschieben
            case SCALE_MATRIX:          glScalef(transform->x, transform->y, transform->z);                     break;  //Skalieren (Multiplikation = vergr��ern/verkleinern)
            case ROTATE_MATRIX:         glRotatef(transform->alpha, transform->x, transform->y, transform->z);  break;  //Drehen
        }
    }
}



void Renderer::renderComposite(Composite *comp){                    //renders all Objects within a composition   
    std::list<Renderable*> const * renderables = comp->getRenderables();
    
    std::stack<const Transformation*> transformations = comp->getTransformations();
    performTransformations(transformations);        //Perform all transformations

    for (std::list<Renderable*>::const_iterator iter = renderables->begin(); iter != renderables->end(); iter++){
        glPushMatrix();

        //Is it a Composite?
        Composite *comp = dynamic_cast<Composite*>(*iter);
        if (comp != NULL){
            renderComposite(comp);
            glPopMatrix();
            continue;
        }
               
        //Is it a Mesh?
        Mesh *mesh = dynamic_cast<Mesh*>(*iter);
        if (mesh != NULL){
            renderMesh(mesh);
            glPopMatrix();
            continue;
        }

        glPopMatrix();
    }
}


void Renderer::renderMesh(Mesh *mesh){              //renders this single Mesh
    renderCounter++;
    std::stack<const Transformation*> transformations = mesh->getTransformations();
    performTransformations(transformations);        //Perform all transformations
    GLint textureId = mesh->getTexture();
    
    setMaterial(mesh->getMaterial(FRONT), mesh->getMaterial(FRONT));

    if (textureId != -1){
        if (Renderer::boundTexture == -1){          //Only change the state if neccessary
            glEnable(GL_TEXTURE_2D);
        }
        glDisable(GL_LIGHTING);
        prepareTexture(textureId);
    }
    else if (Renderer::boundTexture != -1){         //Only change the state if neccessary
        glDisable(GL_TEXTURE_2D);
        Renderer::boundTexture = -1;
    }
   
	const Coord3D* normal = mesh->getNormal();
	glNormal3f(normal->x, normal->y, normal->z);

    glBegin(mesh->getRenderMode());

    std::vector<Vertex*> const * vertices = mesh->getVertices();
    for (std::vector<Vertex*>::const_iterator iter = vertices->begin(); iter != vertices->end(); iter++){
        Color vertexColor = (*iter)->getColor();
        Coord3D vertexPosition = (*iter)->getPosition();   

        glColor4f(vertexColor.r, vertexColor.g, vertexColor.b, vertexColor.a);
        
        if (textureId != -1){
            Coord2D tex = (*iter)->getTextureCoordinates();
            //glTexCoord2f(tex.x, tex.y);
            glTexCoord2f(tex.x, tex.y);
        }

        
        glVertex3f(vertexPosition.x, vertexPosition.y, vertexPosition.z);
    }

    glEnd();    
    glEnable(GL_LIGHTING);	        //Beleuchtung f�r das HUD deaktivieren
}




void Renderer::prepareTexture(GLint textureId){             //Enables or disables texturing (if it isn't already); if enabling: binds the texture (if it isn't bound already) 
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);		//GL_MODULATE: hintergrundfarbe miteinbeziehen; GL_BLEND: �berblenden. GL_DECAL: Nur Textur
    glBindTexture(GL_TEXTURE_2D, textureId);
    Renderer::boundTexture = textureId;
}

void Renderer::addLightSource(LightSource* lightSource){
    lightsources.push_back(lightSource);
}


void Renderer::removeLightSource(LightSource* lightSource){
    lightsources.remove(lightSource);
}