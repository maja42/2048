#pragma once

#include <Windows.h>
#include "glut.h"

#include <stdexcept>
#include <string>

enum AvailableKeys
{
	AK_ArrowUp = 0,
	AK_ArrowDown,
	AK_ArrowLeft,
	AK_ArrowRight,
	AK_W,
	AK_A,
	AK_S,
	AK_D,
	AK_Space,
	AK_ESC,
    AK_1,
    AK_2,
    AK_3,
    AK_4,
    AK_5,
    AK_6,
    AK_7,
    AK_8,
    AK_9,
    AK_0
};

class Keyboard
{
private:
	static Keyboard* _Instance;
	bool* _KeyStates;

	Keyboard();
	virtual ~Keyboard();

	void InitKeyStates();

public:
	const unsigned short KEYSTATES_LENGTH;

	static Keyboard* GetInstance();
	static void StaticKeyboardFunc(unsigned char key, int x, int y);
	static void StaticKeyboardUpFunc(unsigned char key, int x, int y);
	static void StaticSpecialFunc(int key, int x, int y);
	static void StaticSpecialUpFunc(int key, int x, int y);

	bool IsKeyPressed(AvailableKeys key);

private:
	void KeyboardFunc(unsigned char key, int x, int y, bool isDown);
	void SpecialFunc(int key, int x, int y, bool isDown);
};