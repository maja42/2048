#include "Renderable.h"


Renderable::Renderable(){
    clearTransformations();
}

Renderable::Renderable(const Renderable& copyMe){                 //Overwrite the copy-Constructor to perform a deepCopy
    scaleMatrix = copyMe.getScaling();
    rotateMatrix = copyMe.getRotation();
    translateMatrix = copyMe.getTranslation();
}

Renderable::~Renderable()
{}

void Renderable::setTranslation(GLfloat x, GLfloat y, GLfloat z){
    translateMatrix = translation(x, y, z);
}

void Renderable::setScaling(GLfloat x, GLfloat y, GLfloat z){
    scaleMatrix = scaling(x, y, z);
}

void Renderable::setRotation(Axis axis, GLfloat angle){
    rotateMatrix = rotation(axis, angle);
}

void Renderable::clearTransformations(){    
    scaleMatrix = NO_TRANSFORMATION;
    rotateMatrix = NO_TRANSFORMATION;
    translateMatrix = NO_TRANSFORMATION;
}

const Transformation Renderable::getTranslation() const{
    return translateMatrix;
}
const Transformation Renderable::getScaling() const{
    return scaleMatrix;
}
const Transformation Renderable::getRotation() const{
    return rotateMatrix;
}


std::stack<const Transformation*> Renderable::getTransformations() const{     //Returns all transformations
    std::stack<const Transformation*> coll;

    Transformation cmp = NO_TRANSFORMATION;

	if (scaleMatrix != cmp){
		coll.push(&scaleMatrix);
	}
    if (rotateMatrix != cmp){
        coll.push(&rotateMatrix);
	}
    if (translateMatrix != cmp){
        coll.push(&translateMatrix);
    }    
    return coll;
}