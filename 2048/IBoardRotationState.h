#pragma once

class Board;

class IBoardRotationState
{
public:
    virtual void UpdateLogic(Board *context, float elapsedTime) = 0;
};