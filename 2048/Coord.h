#pragma once

#include <stdlib.h>
#include <tuple>
#include <math.h>

#include "glut.h"
//#include <GL/gl.h>  
//#include <GL/glu.h> 




struct Coord2D{
    GLfloat x;
    GLfloat y;
};

struct Coord3D{
    GLfloat x;
    GLfloat y;
    GLfloat z;
};

bool operator == (const Coord2D& a, const Coord2D& b);
bool operator != (const Coord2D& a, const Coord2D& b);
GLfloat distance(const Coord2D& a, const Coord2D& b);

bool operator == (const Coord3D& a, const Coord3D& b);
bool operator != (const Coord3D& a, const Coord3D& b);
GLfloat distance(const Coord3D& a, const Coord3D& b);