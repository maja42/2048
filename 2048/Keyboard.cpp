#include "Keyboard.h"

Keyboard* Keyboard::_Instance = NULL;

//Constructor - Init
Keyboard::Keyboard() : KEYSTATES_LENGTH(20)
{
	this->_KeyStates = new bool[this->KEYSTATES_LENGTH];

	InitKeyStates();
}

Keyboard::~Keyboard()
{
	delete[](this->_KeyStates);
}

void Keyboard::InitKeyStates()
{
	for (unsigned short i = 0; i < this->KEYSTATES_LENGTH; ++i)
	{
		this->_KeyStates[i] = false;
	}
}


//Properties
Keyboard* Keyboard::GetInstance()
{
	if (Keyboard::_Instance == NULL)
	{
		Keyboard::_Instance = new Keyboard();
	}
	return Keyboard::_Instance;
}


//GLUT static callbacks
void Keyboard::StaticKeyboardFunc(unsigned char key, int x, int y)
{
	Keyboard::GetInstance()->KeyboardFunc(key, x, y, true);
}

void Keyboard::StaticKeyboardUpFunc(unsigned char key, int x, int y)
{
	Keyboard::GetInstance()->KeyboardFunc(key, x, y, false);
}

void Keyboard::StaticSpecialFunc(int key, int x, int y)
{
	Keyboard::GetInstance()->SpecialFunc(key, x, y, true);
}

void Keyboard::StaticSpecialUpFunc(int key, int x, int y)
{
	Keyboard::GetInstance()->SpecialFunc(key, x, y, false);
}


//GLUT instance callbacks
void Keyboard::KeyboardFunc(unsigned char key, int x, int y, bool isDown)
{
	switch (key)
	{
	case 'w':
		this->_KeyStates[AK_W] = isDown;
		break;
	case 'a':
		this->_KeyStates[AK_W] = isDown;
		break;
	case 's':
		this->_KeyStates[AK_W] = isDown;
		break;
	case 'd':
		this->_KeyStates[AK_W] = isDown;
		break;
	case ' ':
		this->_KeyStates[AK_Space] = isDown;
		break;
	case 27:
		this->_KeyStates[AK_ESC] = isDown;
		break;

    case '1':
        this->_KeyStates[AK_1] = isDown;
        break;
    case '2':
        this->_KeyStates[AK_2] = isDown;
        break;
    case '3':
        this->_KeyStates[AK_3] = isDown;
        break;
    case '4':
        this->_KeyStates[AK_4] = isDown;
        break;
    case '5':
        this->_KeyStates[AK_5] = isDown;
        break;
    case '6':
        this->_KeyStates[AK_6] = isDown;
        break;
    case '7':
        this->_KeyStates[AK_7] = isDown;
        break;
    case '8':
        this->_KeyStates[AK_8] = isDown;
        break;
    case '9':
        this->_KeyStates[AK_9] = isDown;
        break;
    case '0':
        this->_KeyStates[AK_0] = isDown;
        break;



	}
}

void Keyboard::SpecialFunc(int key, int x, int y, bool isDown)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		this->_KeyStates[AK_ArrowUp] = isDown;
		break;
	case GLUT_KEY_DOWN:
		this->_KeyStates[AK_ArrowDown] = isDown;
		break;
	case GLUT_KEY_LEFT:
		this->_KeyStates[AK_ArrowLeft] = isDown;
		break;
	case GLUT_KEY_RIGHT:
		this->_KeyStates[AK_ArrowRight] = isDown;
		break;
	}
}


bool Keyboard::IsKeyPressed(AvailableKeys key)
{
	if (key >= 0 && key < this->KEYSTATES_LENGTH)
	{
		return this->_KeyStates[key];
	}
	throw new std::overflow_error("Key must be in a range of 0 to " + std::to_string(this->KEYSTATES_LENGTH - 1));
}