#pragma once

//abstract class. serves as an Interface
class Animateable
{
    public:
        virtual void animate(float frameDelay) = 0;
};

