#pragma once

#include "glut.h"

struct LightSource{
    GLfloat position[4];    //Position

    GLfloat diffuse[4];     //Gerichtetes Licht
    GLfloat ambient[4];     //Umgebungslicht
};
