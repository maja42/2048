#include "BoardMiddleState.h"

//Constructor
BoardMiddleState::BoardMiddleState() : TILE_MIN_Y(-1.5f)
{
	this->_TileY = 0.0f;
	this->_IsAtBottom = false;
	this->_MergeCompleted = true;
	this->_IsFirstUpdate = true;
	this->_Sign = -1;
}

BoardMiddleState::~BoardMiddleState()
{}


//IBoardRotationState
void BoardMiddleState::UpdateLogic(Board *context, float elapsedTime)
{

    
	Keyboard *keyboard = Keyboard::GetInstance();

	if (this->_IsFirstUpdate)
	{
		this->_MergeCompleted = !CheckForMerge(context->getTileFields(), context->getTilesLength());

		if (this->_MergeCompleted && context->getTilesMoved())
		{
			CreateTileAtRandom(context);
            this->_IsFirstUpdate = false;
		}
		
	}

    

	if (!this->_MergeCompleted)
	{
        
		MergeTiles(context, elapsedTime);
        
	}
	else
	{
		if (keyboard->IsKeyPressed(AK_ArrowLeft))
		{
			ResetVariables(context);
			context->setState(new BoardLeftState());
			return;
		}
		else if (keyboard->IsKeyPressed(AK_ArrowRight))
		{
			ResetVariables(context);
			context->setState(new BoardRightState());
			return;
		}
		else if (keyboard->IsKeyPressed(AK_ArrowUp))
		{
			ResetVariables(context);
			context->setState(new BoardUpState());
			return;
		}
		else if (keyboard->IsKeyPressed(AK_ArrowDown))
		{
			ResetVariables(context);
			context->setState(new BoardDownState());
			return;
		}
	}
}

void BoardMiddleState::MergeTiles(Board *context, float elapsedTime)
{
	TileField *fields = context->getTileFields();
	int length = context->getTilesLength();

	this->_TileY += (this->_Sign * context->getTileTranslationInc() * elapsedTime / 100.0f);
	if (this->_TileY <= this->TILE_MIN_Y)
	{
		this->_Sign = 1;
		this->_TileY = this->TILE_MIN_Y;
		this->_IsAtBottom = true;
	}
	else if (this->_TileY >= 0.0f)
	{
		this->_TileY = 0.0f;
		this->_MergeCompleted = true;
	}

	for (int i = 0; i < length; ++i)
	{
		TileField *currentField = &fields[i];
		if (currentField->first != NULL && currentField->second != NULL)
		{
			Tile *mergedTile = currentField->first;
			Tile *oldTile = currentField->second;

			Transformation mergedTileTranslation = mergedTile->getTranslation();
			Transformation oldTileTranslation = oldTile->getTranslation();

			mergedTile->setTranslation(mergedTileTranslation.x, this->_TileY, mergedTileTranslation.z);
			oldTile->setTranslation(oldTileTranslation.x, this->_TileY + this->TILE_MIN_Y, oldTileTranslation.z);

			if (this->_IsAtBottom)
			{
				int newTileValue = mergedTile->getValue() * 2;
				mergedTile->setValue(newTileValue);
				context->addScore(newTileValue);

				if (newTileValue >= 2048)
				{
					context->onGameWin();
				}
			}
			if (this->_MergeCompleted)
			{
				context->removeRenderable(oldTile);
				delete(oldTile);
				currentField->second = NULL;
			}
		}
	}

	if (this->_IsAtBottom)
	{
		this->_IsAtBottom = false;
	}
}

void BoardMiddleState::ResetVariables(Board *context)
{
	this->_IsAtBottom = false;
	this->_MergeCompleted = false;
	this->_TileY = 0.0f;
	this->_Sign = -1;
	this->_IsFirstUpdate = true;
	context->setTilesMoved(false);
}

bool BoardMiddleState::CheckForMerge(TileField *fields, int length)
{
	for (int i = 0; i < length; ++i){
		if (fields[i].first != NULL && fields[i].second != NULL)
		{
			return true;
		}
	}
	return false;
}

void BoardMiddleState::CreateTileAtRandom(Board *context)
{

	Coord2D dimenstions = context->getTileDimensions();
	TileField *fields = context->getTileFields();

	if (IsFieldFull(fields, context->getTilesLength()))
	{
		return;
	}

    
	
	int randomX = 0;
	int randomY = 0;

	do
    {
		randomX = rand() % (int)dimenstions.x;
		randomY = rand() % (int)dimenstions.y;
	} while (fields[context->getTileIndex(randomX, randomY)].first != NULL);

    int value = 2;
    if (rand() % 10 == 0){  //Sometimes a 4 appears
        value = 4;
    }

    Tile *createdTile = new Tile(value, { randomX, randomY });  
	fields[context->getTileIndex(randomX, randomY)].first = createdTile;
	context->addRenderable(createdTile);



	if (IsFieldFull(fields, context->getTilesLength()) && !IsAnyTileMoveable(context))
	{
		context->onGameLoose();
	}
}

bool BoardMiddleState::IsFieldFull(TileField *fields, int length)
{
	for (int i = 0; i < length; ++i)
	{
		if (fields[i].first == NULL)
		{
			return false;
		}
	}
	return true;
}

bool BoardMiddleState::IsAnyTileMoveable(Board *context)
{
	TileField *fields = context->getTileFields();
	Coord2D tileDims = context->getTileDimensions();

	for (int x = 0; x < (int)tileDims.x; ++x)
	{
		for (int y = 0; y < (int)tileDims.y; ++y)
		{
			TileField *currentField = &fields[context->getTileIndex(x, y)];
			if (currentField->first == NULL)
			{
				return true;
			}

			if (x - 1 >= 0)
			{
				if (fields[context->getTileIndex(x - 1, y)].first == NULL ||
					fields[context->getTileIndex(x - 1, y)].first->getValue() == currentField->first->getValue())
				{
					std::cout << "x: " << x << " / y: " << y << "\n";
					std::cout << "current: " << currentField->first->getValue() << "\n";
					std::cout << "x - 1: " << fields[context->getTileIndex(x - 1, y)].first->getValue() << "\n\n";
					return true;
				}
			}
			if (x + 1 < (int)tileDims.x)
			{
				if (fields[context->getTileIndex(x + 1, y)].first == NULL ||
					fields[context->getTileIndex(x + 1, y)].first->getValue() == currentField->first->getValue())
				{
					std::cout << "x: " << x << " / y: " << y << "\n";
					std::cout << "current: " << currentField->first->getValue() << "\n";
					std::cout << "x + 1: " << fields[context->getTileIndex(x + 1, y)].first->getValue() << "\n\n";
					return true;
				}
			}
			if (y - 1 >= 0)
			{
				if (fields[context->getTileIndex(x, y - 1)].first == NULL ||
					fields[context->getTileIndex(x, y - 1)].first->getValue() == currentField->first->getValue())
				{
					std::cout << "x: " << x << " / y: " << y << "\n";
					std::cout << "current: " << currentField->first->getValue() << "\n";
					std::cout << "y - 1: " << fields[context->getTileIndex(x, y - 1)].first->getValue() << "\n\n";
					return true;
				}
			}
			if (y + 1 < (int)tileDims.y)
			{
				if (fields[context->getTileIndex(x, y + 1)].first == NULL ||
					fields[context->getTileIndex(x, y + 1)].first->getValue() == currentField->first->getValue())
				{
					std::cout << "x: " << x << " / y: " << y << "\n";
					std::cout << "current: " << currentField->first->getValue() << "\n";
					std::cout << "y + 1: " << fields[context->getTileIndex(x, y + 1)].first->getValue() << "\n\n";
					return true;
				}
			}
		}
	}
	std::cout << "game over..." << std::endl;
	return false;
}