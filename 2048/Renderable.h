#pragma once

#include <string>
#include <stack>

#include "Transformation.h"
#include "Color.h"
#include "Material.h"


//abstract class for the renderer
class Renderable
{
private:
    Transformation scaleMatrix;
    Transformation rotateMatrix;
    Transformation translateMatrix;
public:
    Renderable();
    Renderable::Renderable(const Renderable& copyMe);                 //Overwrite the copy-Constructor to perform a deepCopy
    virtual ~Renderable();   

    void setTranslation(GLfloat x, GLfloat y, GLfloat z);
    void setScaling(GLfloat x, GLfloat y, GLfloat z);
    void setRotation(Axis axis, GLfloat angle);
    
    const Transformation getTranslation() const;
    const Transformation getScaling() const;
    const Transformation getRotation() const;

    virtual void setColor(Color color) = 0;
    virtual void setMaterial(Face face, Material* newMaterial) = 0;


    std::stack<const Transformation*> getTransformations() const;      //Returns all transformations

    void clearTransformations();
};

