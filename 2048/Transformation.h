#pragma once


#include <stdlib.h>
#include <tuple>

#include "glut.h" 
#include <GL/gl.h>  
#include <GL/glu.h> 


enum TransformationType{
    TRANSLATE_MATRIX,
    SCALE_MATRIX,
    ROTATE_MATRIX,
    NONE
};

enum Axis{
    X_AXIS,
    Y_AXIS,
    Z_AXIS
};

struct Transformation{
    GLfloat x;
    GLfloat y;
    GLfloat z;
    GLfloat alpha;

    TransformationType type;
};

#define NO_TRANSFORMATION {1.0, 1.0, 1.0, NONE}

Transformation translation(GLfloat x, GLfloat y, GLfloat z);
Transformation scaling(GLfloat x, GLfloat y, GLfloat z);
Transformation rotation(Axis axis, GLfloat angle);


bool operator == (const Transformation& a, const Transformation& b);
bool operator != (const Transformation& a, const Transformation& b);