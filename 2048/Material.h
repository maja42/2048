#pragma once

#include "glut.h"
#include <GL/gl.h>  
#include <GL/glu.h> 


struct Material{
    GLfloat specular[4];    //gl�nzen
    GLfloat shininess;

    GLfloat diffuse[4];     
    GLfloat ambient[4];     //Umgebungslicht-Filter
    GLfloat emission[4];    //Objekt leuchten
};


enum Face{
    FRONT = GL_FRONT,
    BACK = GL_BACK
};

void setTransparency(Material *material, GLfloat transparency);      //transparency: 0.0 - 1.0
