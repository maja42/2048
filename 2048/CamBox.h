#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "GameComponent.h"
#include "Composite.h"
#include "Mesh.h"
#include "BorderBox2D.h"
#include "Text.h"
#include "config.h"

class CamBox : public Composite, public GameComponent{
    Coord2D boxSize;

    int camNr;
    Text* camText;
    BorderBox2D* camBox;

    float fadeInProgress;
    void updateCamText();
public:
    CamBox(int camNr);
    virtual ~CamBox();

    void setCam(int camNr);
    void update(float elapsedTime);
};