#include "EndBox.h"


EndBox::EndBox(bool victory)
{
    boxSize = ENDBOX_SIZE;
    Coord2D boxPadding = { (MAIN_WINDOW_WIDTH - boxSize.x) / 2, (MAIN_WINDOW_HEIGHT - boxSize.y) / 2 };

    fadeProgress = 0;
    
    Color boxColor = COLOR_WHITE;
    Color textColor = COLOR_WHITE;
    char text[32] = "";
    if (victory){
        strcpy(text, "VICTORY");
        boxColor = { 1.0, 0.85, 0.0, 1.0 };
        textColor = { 0.4, 1.0, 0.4, 1.0 };
    }
    else{
        strcpy(text, "GAME OVER");
        boxColor = { 1.0, 0.0, 0.0, 1.0 };
        textColor = { 1.0, 0.4, 0.2, 1.0 };
    }



    endbox = new BorderBox2D({ MAIN_WINDOW_WIDTH - boxPadding.x - boxSize.x, MAIN_WINDOW_HEIGHT - boxPadding.y - boxSize.y }, boxSize, 30, BRIGHT_BOX);
    endbox->setColor(boxColor);

    this->message = new Text(text, 85, textColor);
    this->message->setTranslation(MAIN_WINDOW_WIDTH - boxPadding.x - boxSize.x / 2 - this->message->getTextWidth()/2, MAIN_WINDOW_HEIGHT - boxPadding.y - boxSize.y / 2 - 42.5, 0);

   
    addRenderable(endbox);
    addRenderableFront(this->message);
    fadeDirection = true;
}


EndBox::~EndBox()
{
}


void EndBox::update(float elapsedTime){
    if (fadeProgress < 100){
        fadeProgress += (elapsedTime / ENDBOX_FADEID_DURATION * 100);
        if (fadeProgress > 100){ fadeProgress = 100; }
        
        if (fadeDirection){
            endbox->setColor({ -1, -1, -1, (fadeProgress / 100.0f)*0.8 });    //Only change the alpha-channel
            message->setColor({ -1, -1, -1, (fadeProgress / 100.0f)*0.8 });    //Only change the alpha-channel
        }
        else{
            endbox->setColor({ -1, -1, -1, 1.0f - (fadeProgress / 100.0f)*0.8 });    //Only change the alpha-channel
            message->setColor({ -1, -1, -1, 1.0f - (fadeProgress / 100.0f)*0.8 });    //Only change the alpha-channel
        }
    }
}

bool EndBox::isFadeEffectFinished(){
    if (fadeProgress >= 100){
        return true;
    }
    return false;
}
void EndBox::fadeOut(){
    fadeProgress = 100.0f - fadeProgress;
    fadeDirection = false;
}