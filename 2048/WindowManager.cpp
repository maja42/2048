#include "WindowManager.h"

WindowManager* WindowManager::singleton = NULL;

WindowManager::WindowManager()
{
	this->elapsedTimeHistory = new short[WindowManager::ELAPSED_TIME_HISTORTY_LENGTH]();
	this->elapsedTimeHistoryIndex = 0;

	this->previousFrameTime = 0L;

	this->averageFrameTime = 1.0f;
}


WindowManager::~WindowManager()
{
	delete[](this->elapsedTimeHistory);
}


WindowManager* WindowManager::getInstance() {
	if (WindowManager::singleton == NULL)
	{
		WindowManager::singleton = new WindowManager();

		glutDisplayFunc(&WindowManager::display);                           //Wird beim rendern aufgerufen
		glutReshapeFunc(&WindowManager::resize);                            //Wird beim resizen des AKTUELLEN Fensters aufgerufen    

		glutKeyboardFunc(&Keyboard::StaticKeyboardFunc);                       //Wird bei einer Tastatureingabe im AKTUELLEN Fenster aufgerufen
		glutKeyboardUpFunc(&Keyboard::StaticKeyboardUpFunc);
		glutSpecialFunc(&Keyboard::StaticSpecialFunc);
		glutSpecialUpFunc(&Keyboard::StaticSpecialUpFunc);
		glutMouseFunc(&WindowManager::mouseFunc);                           //Wird bei einem Tastendruck der Maus aufgerufen
		glutMotionFunc(&WindowManager::mouseMotionFunc);                    //Wird bei der Mausbewegung aufgerufen, wenn eine oder mehr Tasten gedr�ckt sind
		glutPassiveMotionFunc(&WindowManager::mousePassiveMotionFunc);      //Wird bei der Mausbewegung aufgerufen, wenn keine Taste gedr�ckt ist
		glutEntryFunc(&WindowManager::mouseEntryFunc);                      //Wird aufgerufen, wenn ein Fenster den Fokus verliert oder bekommt                     

		glutTimerFunc(16, &WindowManager::staticFpsTimer, 0);
	}
	return WindowManager::singleton;
}


void WindowManager::registerWindow(GlutWindow* newWindow){
    windows.push_back(newWindow);
}


void WindowManager::deregisterWindow(GlutWindow* oldWindow){
    windows.remove(oldWindow);
}



void WindowManager::resize(int width, int height){                     //Wird aufgerufen, wenn irgendein Fenster resized wird
    WindowManager *wm = WindowManager::getInstance();
    GlutWindow *targetWindow = wm->getWindowObject(glutGetWindow());
    targetWindow->resize({ (GLfloat)width, (GLfloat)height });
}


void WindowManager::display(){                                         //Wird beim neu zeichnen von einem beliebigen Fenster aufgerufen
    WindowManager *wm = WindowManager::getInstance();
    GlutWindow *targetWindow = wm->getWindowObject(glutGetWindow());
	targetWindow->render();
}

void WindowManager::staticFpsTimer(int value){
	WindowManager::getInstance()->fpsTimer(value);
	glutTimerFunc(16, &WindowManager::staticFpsTimer, 0);
	glutPostRedisplay();
}

void WindowManager::fpsTimer(int value){
    
	calcAverage();
    //long t = clock();
	for (std::list<GlutWindow*>::iterator i = this->windows.begin(); i != this->windows.end(); ++i)
	{
		(*i)->update(this->averageFrameTime);
		//(*i)->render();
	}    

    //if (clock() - t != 0) { std::cout << "update needs " << clock() - t << "ms\n"; }
}


void WindowManager::calcAverage()
{
	long currentFrameTime = clock();
	short elapsedTime = currentFrameTime - this->previousFrameTime;
	this->previousFrameTime = currentFrameTime;

    //std::cout << elapsedTimeHistoryIndex << "=" << elapsedTime << " -> " << averageFrameTime << "\n";

	if (elapsedTime > 250)  //avoid physics to break
	{
		elapsedTime = 250;
	}
    
	this->elapsedTimeHistory[this->elapsedTimeHistoryIndex] = elapsedTime;
	this->elapsedTimeHistoryIndex = (this->elapsedTimeHistoryIndex + 1) % WindowManager::ELAPSED_TIME_HISTORTY_LENGTH;

	USHORT historySum = 0;
	for (int i = 0; i < WindowManager::ELAPSED_TIME_HISTORTY_LENGTH; ++i)
	{
		historySum += this->elapsedTimeHistory[i];
	}
	this->averageFrameTime = historySum / WindowManager::ELAPSED_TIME_HISTORTY_LENGTH;
}


void WindowManager::mouseFunc(int button, int state, int x, int y){                //Wird bei einem Tastendruck der Maus innerhalb irgendeines Fensters aufgerufen
    WindowManager *wm = WindowManager::getInstance();
    GlutWindow *targetWindow = wm->getWindowObject(glutGetWindow());
    targetWindow->mouseFunc(button, state, WindowManager::convertWindowCoordinates(targetWindow, x, y));
}

void WindowManager::mouseMotionFunc(int x, int y){                              //Wird bei der Mausbewegung innerhalb irgendeines Fensters aufgerufen, wenn eine oder mehr Tasten gedr�ckt sind
    WindowManager* wm = WindowManager::getInstance();
    GlutWindow* targetWindow = wm->getWindowObject(glutGetWindow());

    targetWindow->mouseMotionFunc(WindowManager::convertWindowCoordinates(targetWindow, x, y));
}

void WindowManager::mousePassiveMotionFunc(int x, int y){                      //Wird bei der Mausbewegung innerhalb irgendeines Fensters aufgerufen, wenn keine Taste gedr�ckt ist
    WindowManager *wm = WindowManager::getInstance();
    GlutWindow *targetWindow = wm->getWindowObject(glutGetWindow());
    targetWindow->mousePassiveMotionFunc(WindowManager::convertWindowCoordinates(targetWindow, x, y));
}

void WindowManager::mouseEntryFunc(int state){                                   //Wird aufgerufen, wenn die Maus das Fenster betritt order verl�sst              
    WindowManager *wm = WindowManager::getInstance();
    GlutWindow *targetWindow = wm->getWindowObject(glutGetWindow());
    targetWindow->mouseEntryFunc(state);
}

Coord2D WindowManager::convertWindowCoordinates(GlutWindow* targetWindow, int x, int y){         //Converts Window-Coordinates into correct Matrix-Coordinates
    Coord2D coordinates;
    coordinates.x = x;
    coordinates.y = targetWindow->getSize().y - y;
    return coordinates;
}

void WindowManager::renderAllWindows(){                                        //Renders all Windows
    for (std::list<GlutWindow* >::iterator iter = windows.begin(); iter != windows.end(); iter++){
        (*iter)->render();
    }
}

GlutWindow* WindowManager::getWindowObject(int windowID){               //Returns the window-Object of the given ID
    for (std::list<GlutWindow* >::iterator iter = windows.begin(); iter != windows.end(); iter++){
        if ((*iter)->getWindowID() == windowID){
            return *iter;
        }
    }
	throw std::exception(("Unable to find the Window with ID " + std::to_string(windowID)).c_str());
}
