#include "Text.h"


Tga* Text::fontSprites = new Tga("data/font.tga", { 10, 11 });

const TextSymbol Text::symbols[11][10] = {
    {{'A',0.95}, {'B',0.75}, {'C',0.8 }, {'D',0.84}, {'E',0.72}, {'F',0.7 }, {'G',0.84}, {'H',0.73}, {'I',0.45}, {'J',0.68}},
    {{'K',0.79}, {'L',0.76}, {'M',0.9 }, {'N',0.8 }, {'O',0.85}, {'P',0.75}, {'Q',0.95}, {'R',1.0 }, {'S',0.5 }, {'T',0.83}},
    {{'U',0.8 }, {'V',0.9 }, {'W',1.0 }, {'X',0.78}, {'Y',0.77}, {'Z',0.95}, {' ',0.4 }, {'!',0.25}, {'?',0.42}, {'.',0.24}},
    {{'a',0.5 }, {'b',0.6 }, {'c',0.6 }, {'d',0.6 }, {'e',0.55}, {'f',0.6 }, {'g',0.6 }, {'h',0.55}, {'i',0.23}, {'j',0.4 }},
    {{'k',0.53}, {'l',0.24}, {'m',0.73}, {'n',0.53}, {'o',0.62}, {'p',0.56}, {'q',0.6 }, {'r',0.45}, {'s',0.48}, {'t',0.53}},
    {{'u',0.55}, {'v',0.57}, {'w',0.75}, {'x',0.62}, {'y',0.46}, {'z',0.6 }, {',',0.25}, {':',0.27}, {';',0.29}, {'#',0.72}},
    {{'_',0.63}, {'+',0.52}, {'-',0.34}, {'*',0.46}, {':',0.26}, {'\"',0.35},{'<',0.43}, {'>',0.47}, {'~',0.62}, {'/',0.4 }},
    {{'0',0.6 }, {'1',0.46}, {'2',0.56}, {'3',0.49}, {'4',0.5 }, {'5',0.5 }, {'6',0.47}, {'7',0.58}, {'8',0.5 }, {'9',0.51}},
    {{'\\',0.42},{'=',0.51}, {'(',0.35}, {')',0.25}, {'\'',0.2 },{'%',0.77}, {'�',0.64}, {'�',0.63}, {'@',0.92}, {' ',0.0 }},
    {{'&',0.62}, {'|',0.2 }, {'^',0.5 }, {' ',0.0 }, {' ',0.0 }, {' ',0.0 }, {'[',0.39}, {']',0.38}, {' ',0.0 }, {' ',0.0 }},
    {{'�',0.96}, {'�',0.85}, {'�',0.8 }, {'�',0.49}, {'�',0.61}, {'�',0.55}, {'�',0.5 }, {' ',0.0 }, {' ',0.0 }, {' ',0.0 }}
};


Text::Text(const char* _text, GLfloat _textSize, Color _color){
    strncpy(text, _text, 32);
    color = _color;
    textSize = _textSize;
    fontRatio = 0.75;    //Seitenverh�ltnis der einzelnen Sprites: (zB. 0.5 = Halb so breit wie hoch)
    updateMesh();
}

Text::~Text(){
}

void Text::setText(const char* _text){
    if (strcmp(text, _text) == 0){  //Nothing todo
        return;
    }
    strncpy(text, _text, 32);
    updateMesh();
}

void Text::setColor(Color color){
    if (color.r >= 0){
        this->color.r = color.r;
    }
    if (color.g >= 0){
        this->color.g = color.g;
    }
    if (color.b >= 0){
        this->color.b = color.b;
    }
    if (color.a >= 0){
        this->color.a = color.a;
    }
    Composite::setColor(color);
}

void Text::setFontSize(GLfloat _textSize){
    textSize = _textSize;
    setScaling(textSize * fontRatio, textSize, 0);
}

GLfloat Text::getTextWidth(){     //Returns the width of the printed text
    return textWidth;
}



void Text::updateMesh(){                            //Generates the output
    GLfloat xPos = 0.0;
    textWidth = xPos;
    clearRenderables();
    for (unsigned int i = 0; i < strlen(text); i++){
        xPos = addSymbol(text[i], xPos);
    }
    textWidth = ((xPos - textWidth) * fontRatio)*textSize;
    setScaling(textSize * fontRatio, textSize, 0);
}


GLfloat Text::addSymbol(char letter, GLfloat xPos){   //Adds another letter to the output, and returns the new x-Pos
    GLfloat width = 0;
    
    
    for (int y = 0; y<11 && width == 0; y++){
        for (int x = 0; x<10; x++){
            if (symbols[y][x].symbol == letter){        //gefunden
                fontSprites->setUsedSprite(x, y);
                width = symbols[y][x].width;
                break;
            }
        }
    }
    
    if (width == 0){
        std::cout << "Warning: The letter " << letter << " can't be printed. It is not part of the fontFile." << std::endl;
        return xPos;
    }

 
    Mesh* letterMesh = new Mesh(NULL, NULL, fontSprites->getTextureId());
    
    letterMesh->setRenderMode(GL_QUADS);
    letterMesh->addVertex(new Vertex({ 0.0f, 0.0f, 0.0f }, fontSprites->getTextureCoordinates({ 0.0f, 0.0f }), color));     //bottom left
    letterMesh->addVertex(new Vertex({ 1.0f, 0.0f, 0.0f }, fontSprites->getTextureCoordinates({ 1.0f, 0.0f }), color));     //bottom right
    letterMesh->addVertex(new Vertex({ 1.0f, 1.0f, 0.0f }, fontSprites->getTextureCoordinates({ 1.0f, 1.0f }), color));     //top right
    letterMesh->addVertex(new Vertex({ 0.0f, 1.0f, 0.0f }, fontSprites->getTextureCoordinates({ 0.0f, 1.0f }), color));     //top left
    //letterMesh->setScaling(fontRatio, 1, 1);


    //Left-Bottom
    letterMesh->setTranslation(xPos, 0, 0);
    addRenderable(letterMesh);
    return xPos + width;
}


