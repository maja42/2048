#pragma once
#include "Composite.h"
#include "Mesh.h"
#include "config.h"
#include "Tga.h"

class BoardBorder :  public Composite
{
    private:
        Coord2D size;
        Tga* floorTexture;
    public:
        BoardBorder(Coord2D size);
        ~BoardBorder();
};

