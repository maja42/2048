#include "Composite.h"


Composite::Composite()
{
}


Composite::~Composite(){
    clearRenderables();
}



void Composite::addRenderable(Renderable* renderable){
    childs.push_back(renderable);
}

void Composite::addRenderableFront(Renderable* renderable){
    childs.push_front(renderable);
}

void Composite::removeRenderable(Renderable* renderable){
    childs.remove(renderable);
}

void Composite::clearRenderables(){
    for (std::list<Renderable*>::iterator iter = childs.begin(); iter != childs.end(); ++iter){
        delete *iter;
        *iter = NULL;
    }
    childs.clear();
}

std::list<Renderable*> const* Composite::getRenderables() const{
    return &childs;
}

void Composite::setColor(Color color){
    for (std::list<Renderable*>::iterator iter = childs.begin(); iter != childs.end(); ++iter){
        (*iter)->setColor(color);
    }
}

void Composite::setMaterial(Face face, Material* newMaterial){
    for (std::list<Renderable*>::iterator iter = childs.begin(); iter != childs.end(); ++iter){
        (*iter)->setMaterial(face, newMaterial);
    }
}