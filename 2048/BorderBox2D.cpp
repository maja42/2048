#include "BorderBox2D.h"




Tga* BorderBox2D::boxSprites = new Tga("data/box.tga", { 4, 5 });


BorderBox2D::BorderBox2D(Coord2D pos, Coord2D size, GLfloat borderWidth, BorderBox2dTypes boxType){
 
    //CORNER
    boxSprites->setUsedSprite(0, (int)boxType);
        Mesh* corner = new Mesh(NULL, NULL, boxSprites->getTextureId());
        corner->setRenderMode(GL_QUADS);
        corner->addVertex(new Vertex({ -0.5f, -0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 0.0f, 0.0f }), COLOR_WHITE));        //bottom left
        corner->addVertex(new Vertex({ +0.5f, -0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 1.0f, 0.0f }), COLOR_WHITE));    //bottom right
        corner->addVertex(new Vertex({ +0.5f, +0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 1.0f, 1.0f }), COLOR_WHITE));    //top right
        corner->addVertex(new Vertex({ -0.5f, +0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 0.0f, 1.0f }), COLOR_WHITE));        //top left
        corner->setScaling(borderWidth, borderWidth, 1);
    

        //Left-Bottom
        corner->setTranslation(pos.x + borderWidth / 2, pos.y + borderWidth / 2, 0);
        corner->setRotation(Z_AXIS, 90);
        addRenderable(corner);

        //Left-Top
        corner = new Mesh(*corner);
        corner->setTranslation(pos.x + borderWidth / 2, pos.y + size.y - borderWidth / 2, 0);
        corner->setRotation(Z_AXIS, 0);
        addRenderable(corner);

        //Right-Top
        corner = new Mesh(*corner);
        corner->setTranslation(pos.x + size.x - borderWidth / 2, pos.y + size.y - borderWidth / 2, 0);
        corner->setRotation(Z_AXIS, 270);    
        addRenderable(corner);
    
        //Right-Bottom
        corner = new Mesh(*corner);
        corner->setTranslation(pos.x + size.x - borderWidth / 2, pos.y + borderWidth / 2, 0);
        corner->setRotation(Z_AXIS, 180);
        addRenderable(corner);



    //Hintergrund
    boxSprites->setUsedSprite(3, (int)boxType);
        Mesh* body = new Mesh(NULL, NULL, boxSprites->getTextureId());
        body->setRenderMode(GL_QUADS);
        body->addVertex(new Vertex({ 0.0f, 0.0f, 0.0f }, boxSprites->getTextureCoordinates({ 0.0f, 0.0f }), COLOR_WHITE));      //bottom left
        body->addVertex(new Vertex({ 1.0f, 0.0f, 0.0f }, boxSprites->getTextureCoordinates({ 1.0f, 0.0f }), COLOR_WHITE));      //bottom right
        body->addVertex(new Vertex({ 1.0f, 1.0f, 0.0f }, boxSprites->getTextureCoordinates({ 1.0f, 1.0f }), COLOR_WHITE));      //top right
        body->addVertex(new Vertex({ 0.0f, 1.0f, 0.0f }, boxSprites->getTextureCoordinates({ 0.0f, 1.0f }), COLOR_WHITE));      //top left
        body->setScaling(size.x - 2 * borderWidth, size.y - 2 * borderWidth, 1);
        body->setTranslation(pos.x + borderWidth, pos.y + borderWidth, 0);
        addRenderable(body);


    //Horizontal Edges
        boxSprites->setUsedSprite(1, (int)boxType);

        //Top
        Mesh* hEdge = new Mesh(NULL, NULL, boxSprites->getTextureId());
        hEdge->setRenderMode(GL_QUADS);
        hEdge->addVertex(new Vertex({ -0.5f, -0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 0.0f, 0.0f }), COLOR_WHITE));        //bottom left
        hEdge->addVertex(new Vertex({ +0.5f, -0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 1.0f, 0.0f }), COLOR_WHITE));    //bottom right
        hEdge->addVertex(new Vertex({ +0.5f, +0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 1.0f, 1.0f }), COLOR_WHITE));    //top right
        hEdge->addVertex(new Vertex({ -0.5f, +0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 0.0f, 1.0f }), COLOR_WHITE));        //top left
        hEdge->setScaling(size.x - 2 * borderWidth, borderWidth, 1);

        hEdge->setTranslation(pos.x + size.x / 2, pos.y + borderWidth / 2, 0);
        hEdge->setRotation(Z_AXIS, 180);
        addRenderable(hEdge);

        //Bottom
        hEdge = new Mesh(*hEdge);
        hEdge->setTranslation(pos.x + size.x / 2, pos.y - borderWidth / 2 + size.y, 0);
        hEdge->setRotation(Z_AXIS, 0);
        addRenderable(hEdge);


    //Vertical Edges
    boxSprites->setUsedSprite(2, (int)boxType);
        //Left
        Mesh* vEdge = new Mesh(NULL, NULL, boxSprites->getTextureId());
        vEdge->setRenderMode(GL_QUADS);
        vEdge->addVertex(new Vertex({ -0.5f, -0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 0.0f, 0.0f }), COLOR_WHITE));        //bottom left
        vEdge->addVertex(new Vertex({ +0.5f, -0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 1.0f, 0.0f }), COLOR_WHITE));    //bottom right
        vEdge->addVertex(new Vertex({ +0.5f, +0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 1.0f, 1.0f }), COLOR_WHITE));    //top right
        vEdge->addVertex(new Vertex({ -0.5f, +0.5f, 0.0f }, boxSprites->getTextureCoordinates({ 0.0f, 1.0f }), COLOR_WHITE));        //top left
        vEdge->setScaling(borderWidth, size.y - 2 *  borderWidth, 1);

        vEdge->setTranslation(pos.x + borderWidth / 2, pos.y + size.y / 2, 0);
        vEdge->setRotation(Z_AXIS, 0);
        addRenderable(vEdge);

        //Right
        vEdge = new Mesh(*vEdge);
        vEdge->setTranslation(pos.x + size.x - borderWidth / 2, pos.y + size.y / 2, 0);
        vEdge->setRotation(Z_AXIS, 180);
        addRenderable(vEdge);

}


BorderBox2D::~BorderBox2D(){
}
