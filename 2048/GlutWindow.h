#pragma once

#include <stdlib.h>

#include "glut.h" 
#include <GL/gl.h>  
#include <GL/glu.h>  

#include "WindowManager.h"
#include "Renderer.h"
#include "Coord.h"
#include "Color.h"

//Abstract Class for Glut OpenGl-Windows
class GlutWindow
{
private:
    int windowID;

    Coord2D windowSize;     //Size of the window
    Coord2D size;           //Coordinate-Size (NOT the window-size)
    Color clearColor;       //Default Window-Backgroundcolor

protected:
    Renderer* renderer = NULL;                                      //Renderer, that is used for this Window
public:
    GlutWindow(Coord2D position, Coord2D windowSize, const char* windowTitle, Color clearColor = COLOR_BLACK);
    virtual ~GlutWindow();

    int getWindowID();          //Returns the id of the glut-Window
    Coord2D getSize();            //Returns the size of the coord.-system (is NOT the window-size)
    
    void render() const;                                              //Wird beim neu zeichnen aufgerufen
    void setCursor(int cursor);                                       //�ndert den Cursor in diesem Fenster
	virtual void update(float elapsedTime) = 0;

    //Glut-Functions:
    virtual void resize(Coord2D newSize);                               //Wird aufgerufen, wenn das Fenster resized wird
    virtual void mouseFunc(int button, int state, Coord2D position);    //Wird bei einem Tastendruck der Maus innerhalb des Fensters aufgerufen
    virtual void mouseMotionFunc(Coord2D position);                     //Wird bei der Mausbewegung innerhalb des Fensters aufgerufen, wenn eine oder mehr Tasten gedr�ckt sind
    virtual void mousePassiveMotionFunc(Coord2D position);              //Wird bei der Mausbewegung innerhalb des Fensters aufgerufen, wenn keine Taste gedr�ckt ist
    virtual void mouseEntryFunc(int state);                             //Wird aufgerufen, wenn das Fenster den Fokus verliert oder bekommt
};

